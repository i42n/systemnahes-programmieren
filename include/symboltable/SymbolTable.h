#ifndef SYMBOLTABLE_H_
#define SYMBOLTABLE_H_

#include "scanner/Information.h"
#include "symboltable/StringTable.h"
#include "symboltable/SymbolTableEntry.h"
#include "scanner/Token.h"

#define TABLE_SIZE 1024

class SymbolTable {
	public:
		SymbolTable();
		virtual ~SymbolTable();
		SymbolTableEntry* addLexem(const char* lexem, int lexem_length, Token::Type token_type);

	private:
		StringTable* stringTable;
		SymbolTableEntry* table [TABLE_SIZE];
		void initSymbols();

		unsigned int hash(const char* string, unsigned int seed);
		SymbolTableEntry* create_new_entry(const char* lexem, int lexem_length, Token::Type token_type);
};

#endif /* SYMBOLTABLE_H_ */

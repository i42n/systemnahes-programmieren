#ifndef STRINGTABLE_H_
#define STRINGTABLE_H_

// this should at min be the same size as the scanner buffer size!
#define ALLOC_SIZE 40000

typedef struct StringTableEntry {
	char data [ALLOC_SIZE];
	int space_left;
	StringTableEntry* next;
} StringTableEntry;

class StringTable {
	public:
		StringTable();
		virtual ~StringTable();
		char* insert(const char* lexem, int lexem_length);
	private:
		char* nextFreeSpace;
		StringTableEntry* current_entry;
		StringTableEntry* first_entry;
		void increaseSize();
};

#endif /* STRINGTABLE_H_ */

#ifndef SYMBOLTABLEENTRY_H_
#define SYMBOLTABLEENTRY_H_

#include "scanner/Information.h"
#include "scanner/Token.h"

class SymbolTableEntry {
	public:
		SymbolTableEntry(Token::Type token_type, Information* information);
		virtual ~SymbolTableEntry();
		SymbolTableEntry* getNext() const;
		void setNext(SymbolTableEntry* next);
		Information* getInformation() const;
		Token::Type getType() const;
	private:
		Information* information;
		SymbolTableEntry* next;
		Token::Type token_type;
};

#endif /* SYMBOLTABLEENTRY_H_ */

// display debug messages and exit program
#include <stdlib.h>
#include <iostream>

// print message and quit program
#define STDERR(x) do{std::cerr << x << std::endl; exit(EXIT_FAILURE);} while(0)

//print message
#define STDMESSAGE(x) (std::cout << x << std::endl)

//#define DEBUG

// display debug message and continue executing program
#ifdef DEBUG
#define DEBUG_STDOUT(x) (std::cout << x << std::endl)

#else
#define DEBUG_STDOUT(x)

#endif
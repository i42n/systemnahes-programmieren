#ifndef VISITOR_H
#define VISITOR_H

class NodeProg;
class NodeDecls;
class NodeDecl;
class NodeArray;
class NodeStatements;
class NodeStatement;
class NodeStatementAssign;
class NodeStatementBlock;
class NodeStatementIfElse;
class NodeStatementPrint;
class NodeStatementRead;
class NodeStatementWhile;
class NodeExp;
class NodeExp2;
class NodeExp2Exp;
class NodeExp2IdentifierIndex;
class NodeExp2Integer;
class NodeExp2NegativeExp;
class NodeExp2NotExp;
class NodeIndex;
class NodeOpExp;
class NodeOp;

class NodeDeclsEpsilon;
class NodeArrayEpsilon;
class NodeStatementsEpsilon;
class NodeOpExpEpsilon;
class NodeIndexEpsilon;

class NodeLeaf;
class NodeIdentifier;
class NodeInteger;
class Node;

class Visitor {
	public:

		virtual void visit(Node* node) = 0;
		virtual void visit(NodeStatement* node) = 0;
		virtual void visit(NodeLeaf* node) = 0;
		virtual void visit(NodeExp2* node) = 0;

		virtual void visit(NodeProg* node) = 0;
		virtual void visit(NodeDecls* node) = 0;
		virtual void visit(NodeDecl* node) = 0;
		virtual void visit(NodeArray* node) = 0;
		virtual void visit(NodeStatements* node) = 0;
		virtual void visit(NodeStatementAssign* node) = 0;
		virtual void visit(NodeStatementPrint* node) = 0;
		virtual void visit(NodeStatementRead* node) = 0;
		virtual void visit(NodeStatementBlock* node) = 0;
		virtual void visit(NodeStatementIfElse* node) = 0;
		virtual void visit(NodeStatementWhile* node) = 0;

		virtual void visit(NodeExp* node) = 0;
		virtual void visit(NodeExp2Exp* node) = 0;
		virtual void visit(NodeExp2IdentifierIndex* node) = 0;
		virtual void visit(NodeExp2Integer* node) = 0;
		virtual void visit(NodeExp2NegativeExp* node) = 0;
		virtual void visit(NodeExp2NotExp* node) = 0;

		virtual void visit(NodeIndex* node) = 0;
		virtual void visit(NodeOpExp* node) = 0;
		virtual void visit(NodeOp* node) = 0;

		virtual void visit(NodeIdentifier* node) = 0;
		virtual void visit(NodeInteger* node) = 0;

		virtual void visit(NodeDeclsEpsilon* node) = 0;
		virtual void visit(NodeArrayEpsilon* node) = 0;
		virtual void visit(NodeStatementsEpsilon* node) = 0;
		virtual void visit(NodeOpExpEpsilon* node) = 0;
		virtual void visit(NodeIndexEpsilon* node) = 0;
};

#endif /* VISITOR_H */
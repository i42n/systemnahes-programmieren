#include "scanner/Scanner.h"
#include "scanner/Token.h"
#include "parser/ParseTree.h"
#include "parser/node/Node.h"

#ifndef PARSER_H
#define PARSER_H

class Parser {
	public:
		Parser(const char* filename);
		virtual ~Parser();
		ParseTree* processFile();

	private:
		Scanner* scanner;
		Token* currentToken;
		Token* lastToken;

		void syntax_error(Token::Type expected_type);
		void syntax_error(const char* expected_type);


		bool tokenCheck(Token::Type type);
		void match(Token::Type token_type);
		void next();

		NodeProg* prog();
		NodeDecls* decls();
		NodeDecl* decl();
		NodeArray* array();
		NodeStatements* statements();
		NodeStatement* statement();
		NodeExp* exp();
		NodeExp2* exp2();
		NodeIndex* index();
		NodeOpExp* op_exp();
		NodeOp* op();
};

#endif /* PARSER_H */
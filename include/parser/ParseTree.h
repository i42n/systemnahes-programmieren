#ifndef PARSE_TREE_H
#define PARSE_TREE_H

#include "parser/node/Node.h"

class ParseTree {
	public:
		ParseTree(NodeProg* prog);
		virtual ~ParseTree();

		NodeProg* getProg();
		
	private:
		NodeProg* prog;

};

#endif /* PARSE_TREE_H */
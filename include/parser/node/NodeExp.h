#ifndef NodeExp_H_
#define NodeExp_H_

#include "parser/node/NodeExp2.h"
#include "parser/node/NodeOpExp.h"

class NodeExp : public Node {

		NodeExp2* expression;
		NodeOpExp* operatorExpression;

	public:
		NodeExp() {
			expression = NULL;
			operatorExpression = NULL;
		}

		virtual ~NodeExp() {
			delete expression;
			delete operatorExpression;
		}

		virtual NodeExp2* getExpression() {
			return this->expression;
		}

		virtual NodeOpExp* getOperatorExpression() {
			return this->operatorExpression;
		}

		virtual void addChild(NodeExp2* newExpression) {
			this->expression = newExpression;
		}

		virtual void addChild(NodeOpExp* newOperatorExpression) {
			this->operatorExpression = newOperatorExpression;
		}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif

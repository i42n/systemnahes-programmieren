#ifndef NodeStatementAssign_H_
#define NodeStatementAssign_H_

#include "parser/node/NodeStatement.h"
#include "parser/node/NodeIdentifier.h"
#include "parser/node/NodeIndex.h"
#include "parser/node/NodeExp.h"

class NodeStatementAssign : public NodeStatement {

		NodeIdentifier* identifier;
		NodeIndex* index;
		NodeExp* expression;

	public:
		NodeStatementAssign() {
			identifier = 0;
			expression = 0;
			index = 0;
		}

		virtual ~NodeStatementAssign() {
			delete identifier;
			delete expression;
			delete index;
		}

		virtual NodeIdentifier* getIdentifier() {
			return this->identifier;
		}

		virtual NodeExp* getExpression() {
			return this->expression;
		}

		virtual NodeIndex* getIndex() {
			return this->index;
		}

		virtual void addChild(NodeIdentifier* newIdentifier) {
			this->identifier = newIdentifier;
		}

		virtual void addChild(NodeExp* newExpression) {
			this->expression = newExpression;
		}

		virtual void addChild(NodeIndex* newIndex) {
			this->index = newIndex;
		}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif /* NodeStatementAssign_H_ */

#ifndef NodeExp2NegativeExp_H_
#define NodeExp2NegativeExp_H_

#include "parser/node/NodeExp2.h"

class NodeExp2NegativeExp : public NodeExp2 {

		NodeExp2* expression;

	public:
		NodeExp2NegativeExp() {
			expression = NULL;
		}

		virtual ~NodeExp2NegativeExp() {
			delete expression;
		}

		virtual NodeExp2* getExpression() {
			return expression;
		}

		virtual void addChild(NodeExp2* newExpression) {
			expression = newExpression;
		}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif

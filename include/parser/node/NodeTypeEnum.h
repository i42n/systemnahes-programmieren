#ifndef NODE_TYPE_ENUM
#define NODE_TYPE_ENUM

enum NodeType {
	TYPE_NONE = 0,				/* not type set yet */
	TYPE_INTEGER,				/* integer */
	TYPE_INTEGER_ARRAY,			/* integer array type */
	TYPE_ARRAY,					/* array type */
	TYPE_ERROR,					/* error type - invalid type */
	TYPE_OP_PLUS,				/* '+' Plus */
	TYPE_OP_MINUS,				/* '-' Minus */
	TYPE_OP_DIVIDE,				/* '/' Division */
	TYPE_OP_MULTIPLY,			/* '*' Multiplication */
	TYPE_OP_GREATER,			/* '>' Greater */
	TYPE_OP_SMALLER,			/* '<' Smaller */
	TYPE_OP_UNEQUAL,			/* '=!=' Unequals */
	TYPE_OP_COMPARE,			/* '==' Compare */
	TYPE_OP_AND,				/* '&' Logical AND */

	ENUM_END_MARKER // marker to know how many elements the enum contains
};

#endif /* NODE_TYPE_ENUM */
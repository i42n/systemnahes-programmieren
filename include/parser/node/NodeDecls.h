#ifndef NodeDecls_H_
#define NodeDecls_H_


#include "parser/node/NodeDecl.h"

class NodeDecls: public Node {

		NodeDecl* declaration;
		NodeDecls* nextDeclarations;

	public:
		NodeDecls() {
			declaration = 0;
			nextDeclarations = 0;
		}

		virtual ~NodeDecls() {
			delete declaration;
			delete nextDeclarations;
		}

		virtual NodeDecl* getDeclaration() {
			return this->declaration;
		}

		virtual NodeDecls* getNextDeclarations() {
			return this->nextDeclarations;
		}

		virtual void addChild(NodeDecl* newDeclaration) {
			this->declaration = newDeclaration;
		}

		virtual void addChild(NodeDecls* newDeclarations) {
			this->nextDeclarations = newDeclarations;
		}
		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif

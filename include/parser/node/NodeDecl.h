#ifndef NODEDECL_H_
#define NODEDECL_H_


#include "parser/node/NodeArray.h"
#include "parser/node/NodeIdentifier.h"

class NodeDecl : public Node {

		NodeArray* array;
		NodeIdentifier* identifier;

	public:
		NodeDecl(void) {
			array = 0;
			identifier = 0;
		}

		virtual ~NodeDecl(void) {
			delete array;
			delete identifier;
		}

		virtual NodeIdentifier* getIdentifier() {
			return this->identifier;
		}

		virtual NodeArray* getArray() {
			return this->array;
		}

		virtual void addChild(NodeArray* newArray) {
			this->array = newArray;
		}

		virtual void addChild(NodeIdentifier* identifier) {
			this->identifier = identifier;
		}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};
#endif

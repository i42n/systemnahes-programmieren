#ifndef NodeExp2Integer_H_
#define NodeExp2Integer_H_

#include "parser/node/NodeExp2.h"
#include "parser/node/NodeInteger.h"

class NodeExp2Integer : public NodeExp2 {

		NodeInteger* integer;

	public:
		NodeExp2Integer() {
			integer = NULL;
		}

		virtual ~NodeExp2Integer() {
			delete integer;
		}

		virtual NodeInteger* getInteger() {
			return this->integer;
		}

		virtual void addChild(NodeInteger* integer) {
			this->integer = integer;
		}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif

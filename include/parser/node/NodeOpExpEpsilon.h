#ifndef NodeOpExpEpsilon_H_
#define NodeOpExpEpsilon_H_

class NodeOpExpEpsilon: public NodeOpExp {

	public:
		NodeOpExpEpsilon() {}
		virtual ~NodeOpExpEpsilon() {}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif

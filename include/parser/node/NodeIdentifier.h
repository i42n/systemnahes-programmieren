#ifndef NodeIdentifier_H_
#define NodeIdentifier_H_

#include "parser/node/NodeLeaf.h"
#include "scanner/Token.h"
#include "scanner/Information.h"

class NodeIdentifier : public NodeLeaf {	

	public:
		NodeIdentifier(Token* token) {
			this->token = token;
		}

		virtual ~NodeIdentifier() {
		}
		

		virtual void setType(NodeType newType) {
			return this->token->getInformation()->setType(newType);
		}
		
		virtual NodeType getType(){
			return this->token->getInformation()->getType();
		}

		virtual const char* getLexem() {
			return token->getInformation()->getLexem();
		}
		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
		
		virtual int getLine(){
			return token->getLine();
		}
		virtual int getColumn(){
			return token->getColumn();
		}
};

#endif /* NodeIdentifier_H_ */

#ifndef NodeProg_H_
#define NodeProg_H_

#include "parser/node/Node.h"
#include "parser/node/NodeDecls.h"
#include "parser/node/NodeStatements.h"

class NodeProg : public Node {

		NodeDecls* declarations;
		NodeStatements* statements;

	public:
		NodeProg() {
			declarations = NULL;
			statements = NULL;
		}

		virtual ~NodeProg() {
			delete declarations;
			delete statements;
		}

		virtual NodeDecls* getDeclarations() {
			return declarations;
		}

		virtual NodeStatements* getStatements() {
			return statements;
		}

		virtual void addChild(NodeDecls* newDeclarations) {
			declarations = newDeclarations;
		}

		virtual void addChild(NodeStatements* newStatements) {
			statements = newStatements;
		}
		
		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif /* NodeProg_H_ */

#ifndef NodeStatementWhile_H_
#define NodeStatementWhile_H_

#include "parser/node/NodeStatement.h"
#include "parser/node/NodeExp.h"
#include "parser/node/NodeStatement.h"

class NodeStatementWhile : public NodeStatement {

		NodeExp* expression;
		NodeStatement* statement;

	public:
		NodeStatementWhile() {
			expression = NULL;
			statement = NULL;
		}

		virtual ~NodeStatementWhile() {
			delete expression;
			delete statement;
		}

		virtual NodeExp* getExpression() {
			return this->expression;
		}

		virtual NodeStatement* getStatement() {
			return this->statement;
		}

		virtual void addChild(NodeExp* newExpression) {
			this->expression = newExpression;
		}

		virtual void addChild(NodeStatement* newStatement) {
			this->statement = newStatement;
		}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif /* NodeStatementWhile_H_ */

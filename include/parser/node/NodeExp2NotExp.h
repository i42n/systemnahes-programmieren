#ifndef NodeExp2NotExp_H_
#define NodeExp2NotExp_H_

#include "parser/node/NodeExp2.h"

class NodeExp2NotExp : public NodeExp2 {

		NodeExp2* expression;

	public:
		NodeExp2NotExp() {
			expression = NULL;
		}

		virtual ~NodeExp2NotExp() {
			delete expression;
		}

		virtual NodeExp2* getExpression() {
			return this->expression;
		}

		virtual void addChild(NodeExp2* newExpression) {
			this->expression = newExpression;
		}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif

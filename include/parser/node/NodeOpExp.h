#ifndef NodeOpExp_H_
#define NodeOpExp_H_

#include "parser/node/NodeOp.h"

class NodeExp;

class NodeOpExp : public Node {

		NodeExp* expression;
		NodeOp* operation;

	public:
		NodeOpExp() {
			expression = NULL;
			operation = NULL;
		}

		virtual ~NodeOpExp() {
			delete expression;
			delete operation;
		}

		virtual NodeExp* getExpression() {
			return expression;
		}

		virtual NodeOp* getOperation() {
			return operation;
		}

		virtual void addChild(NodeExp* newExpression) {
			expression = newExpression;
		}

		virtual void addChild(NodeOp* newOperation) {
			operation = newOperation;
		}
		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif /* NodeOpExp_H_ */

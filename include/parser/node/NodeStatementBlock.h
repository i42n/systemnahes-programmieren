#ifndef NodeStatementBlock_H_
#define NodeStatementBlock_H_

#include "parser/node/NodeStatements.h"

class NodeStatementBlock: public NodeStatement {

		NodeStatements* statements;

	public:
		NodeStatementBlock() {
			statements = 0;
		}

		virtual ~NodeStatementBlock() {
			delete statements;
		}

		virtual NodeStatements* getStatements() {
			return this->statements;
		}

		virtual void addChild(NodeStatements* newStatements) {
			this->statements = newStatements;
		}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif /* NodeStatementBlock_H_ */

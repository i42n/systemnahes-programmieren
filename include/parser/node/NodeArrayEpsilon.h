#ifndef NodeArrayEpsilon_H_
#define NodeArrayEpsilon_H_

class NodeArrayEpsilon: public NodeArray {

	public:
		NodeArrayEpsilon() {}
		virtual ~NodeArrayEpsilon() {}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif

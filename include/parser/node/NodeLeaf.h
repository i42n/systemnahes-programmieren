#ifndef NodeLeaf_H_
#define NodeLeaf_H_


#include "scanner/Token.h"

class NodeLeaf : public Node {
	protected:

		Token* token;
		NodeLeaf();

	public:
		virtual ~NodeLeaf();

		virtual int getLine() {
			return token->getLine();
		}

		virtual int getColumn() {
			return token->getColumn();
		}
		virtual void accept(Visitor* visitor);
};

#endif /* NodeLeaf_H_ */

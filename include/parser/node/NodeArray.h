#ifndef NODEARRAY_H_
#define NODEARRAY_H_


#include "parser/node/NodeInteger.h"

class NodeArray : public Node {
	
	 	NodeInteger* integer;

	public:
		NodeArray() {
			this->integer = 0;
		}

		virtual ~NodeArray() {
			delete this->integer;
		}

		virtual void addChild(NodeInteger* newInteger) {
			this->integer = newInteger;
		}

		virtual NodeInteger* getInteger() {
			return this->integer;
		}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif

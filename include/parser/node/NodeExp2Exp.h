#ifndef NodeExp2Exp_H_
#define NodeExp2Exp_H_

#include "parser/node/NodeExp2.h"
#include "parser/node/NodeExp.h"

class NodeExp2Exp : public NodeExp2 {

		NodeExp* expression;

	public:
		NodeExp2Exp() {
			expression = NULL;
		}

		virtual ~NodeExp2Exp() {
			delete expression;
		}

		virtual NodeExp* getExpression() {
			return this->expression;
		}

		virtual void addChild(NodeExp* newExpression) {
			this->expression = newExpression;
		}
		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif

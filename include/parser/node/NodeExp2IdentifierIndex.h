#ifndef NodeExp2IdentifierIndex_H_
#define NodeExp2IdentifierIndex_H_

#include "parser/node/NodeExp2.h"
#include "parser/node/NodeIdentifier.h"
#include "parser/node/NodeIndex.h"

class NodeExp2IdentifierIndex : public NodeExp2 {

		NodeIdentifier* identifier;
		NodeIndex* index;

	public:
		NodeExp2IdentifierIndex() {
			identifier = NULL;
			index = NULL;
		}

		virtual ~NodeExp2IdentifierIndex() {
			delete identifier;
			delete index;
		}

		virtual NodeIdentifier* getIdentifier() {
			return this->identifier;
		}

		virtual NodeIndex* getIndex() {
			return this->index;
		}

		virtual void addChild(NodeIdentifier* identifier) {
			this->identifier = identifier;
		}

		virtual void addChild(NodeIndex* newIndex) {
			this->index = newIndex;
		}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif

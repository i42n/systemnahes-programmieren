#ifndef NodeInteger_H_
#define NodeInteger_H_

#include "parser/node/NodeLeaf.h"
#include "scanner/Token.h"

class NodeInteger : public NodeLeaf {

		long int value;

	public:
		NodeInteger(Token* token) {
			this->value = token->getValue();
			this->token = token;
		}

		virtual ~NodeInteger() {}

		virtual long int getValue() {
			return value;
		}
		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
		
		virtual int getLine(){
			return token->getLine();
		}
		virtual int getColumn(){
			return token->getColumn();
		}
};

#endif /* NodeInteger_H_ */

#ifndef NodeStatement_H_
#define NodeStatement_H_

class NodeStatement : public Node {
	protected:
		NodeStatement();
	public:
		virtual ~NodeStatement();
		virtual void accept(Visitor* visitor);
};

#endif /* NodeStatement_H_ */

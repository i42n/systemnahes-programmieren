#ifndef NODE_H_
#define NODE_H_

#include <stdlib.h>
#include "parser/Visitor.h"
#include "parser/node/NodeTypeEnum.h"

class Node {
	public:

		// string representation of the Token::NodeType enum
		static const char* node_type_as_string(NodeType type) {
			const char* node_type_as_string[ENUM_END_MARKER] = {
				"TYPE_NONE",				/* not type set yet */
				"TYPE_INTEGER",				/* integer */
				"TYPE_INTEGER_ARRAY",		/* integer array type */
				"TYPE_ARRAY",				/* array type */
				"TYPE_ERROR",				/* error type - invalid type */
				"TYPE_OP_PLUS",				/* '+' Plus */
				"TYPE_OP_MINUS",			/* '-' Minus */
				"TYPE_OP_DIVIDE",			/* '/' Division */
				"TYPE_OP_MULTIPLY",			/* '*' Multiplication */
				"TYPE_OP_GREATER",			/* '>' Greater */
				"TYPE_OP_SMALLER",			/* '<' Smaller */
				"TYPE_OP_UNEQUAL",			/* '=!=' Unequals */
				"TYPE_OP_COMPARE",			/* '==' Compare */
				"TYPE_OP_AND",				/* '&' Logical AND */
			};
			return node_type_as_string[type];
		}

		virtual ~Node(void);
		virtual void setType(NodeType newType);
		virtual NodeType getType();
		virtual void addChild(Node* node);
		virtual void accept(Visitor* visitor);

	protected:
		NodeType nodeType;
		Node();
};

#include "parser/node/NodeProg.h"
#include "parser/node/NodeDecls.h"
#include "parser/node/NodeDecl.h"
#include "parser/node/NodeArray.h"
#include "parser/node/NodeStatements.h"
#include "parser/node/NodeStatement.h"
#include "parser/node/NodeStatementAssign.h"
#include "parser/node/NodeStatementPrint.h"
#include "parser/node/NodeStatementRead.h"
#include "parser/node/NodeStatementBlock.h"
#include "parser/node/NodeStatementIfElse.h"
#include "parser/node/NodeStatementWhile.h"

#include "parser/node/NodeExp.h"
#include "parser/node/NodeExp2.h"
#include "parser/node/NodeExp2Exp.h"
#include "parser/node/NodeExp2IdentifierIndex.h"
#include "parser/node/NodeExp2Integer.h"
#include "parser/node/NodeExp2NegativeExp.h"
#include "parser/node/NodeExp2NotExp.h"

#include "parser/node/NodeIndex.h"
#include "parser/node/NodeOpExp.h"
#include "parser/node/NodeOp.h"

#include "parser/node/NodeLeaf.h"
#include "parser/node/NodeIdentifier.h"
#include "parser/node/NodeInteger.h"

#include "parser/node/NodeDeclsEpsilon.h"
#include "parser/node/NodeArrayEpsilon.h"
#include "parser/node/NodeStatementsEpsilon.h"
#include "parser/node/NodeIndexEpsilon.h"
#include "parser/node/NodeOpExpEpsilon.h"

#endif

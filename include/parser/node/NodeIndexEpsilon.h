#ifndef NodeIndexEpsilon_H_
#define NodeIndexEpsilon_H_

class NodeIndexEpsilon: public NodeIndex {

	public:
		NodeIndexEpsilon() {}
		virtual ~NodeIndexEpsilon() {}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif

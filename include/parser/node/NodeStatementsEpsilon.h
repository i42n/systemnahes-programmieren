#ifndef NodeStatementsEpsilon_H_
#define NodeStatementsEpsilon_H_

class NodeStatementsEpsilon: public NodeStatements {

	public:
		NodeStatementsEpsilon() {}
		virtual ~NodeStatementsEpsilon() {}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif

#ifndef NodeStatementPrint_H_
#define NodeStatementPrint_H_

#include "parser/node/NodeStatement.h"
#include "parser/node/NodeExp.h"

class NodeStatementPrint : public NodeStatement {

		NodeExp* expression;

	public:
		NodeStatementPrint(void) {
			expression = 0;
		}

		virtual ~NodeStatementPrint(void) {
			delete expression;
		}

		virtual NodeExp* getExpression() {
			return this->expression;
		}

		virtual void addChild(NodeExp* newExpression) {
			this->expression = newExpression;
		}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif /* NodeStatementPrint_H_ */

#ifndef NodeIndex_H_
#define NodeIndex_H_


#include "parser/node/NodeExp.h"

class NodeIndex : public Node {
	
		NodeExp* expression;

	public:
		NodeIndex() {
			expression = NULL;
		}

		virtual ~NodeIndex() {
			delete expression;
		}

		virtual NodeExp* getExpression() {
			return this->expression;
		}

		virtual void addChild(NodeExp* newExpression) {
			this->expression = newExpression;
		}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};


#endif /* NodeIndex_H_ */

#ifndef NodeStatementRead_H_
#define NodeStatementRead_H_

#include "parser/node/NodeStatement.h"
#include "parser/node/NodeIdentifier.h"
#include "parser/node/NodeIndex.h"

class NodeStatementRead : public NodeStatement {

		NodeIdentifier* identifier;
		NodeIndex* index;

	public:
		NodeStatementRead() {
			identifier = NULL;
			index = NULL;
		}

		virtual ~NodeStatementRead() {
			delete identifier;
			delete index;
		}

		virtual NodeIdentifier* getIdentifier() {
			return this->identifier;
		}

		virtual NodeIndex* getIndex() {
			return this->index;
		}

		virtual void addChild(NodeIdentifier* newIdentifier) {
			this->identifier = newIdentifier;
		}

		virtual void addChild(NodeIndex* newIndex) {
			this->index = newIndex;
		}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif /* NodeStatementRead_H_ */

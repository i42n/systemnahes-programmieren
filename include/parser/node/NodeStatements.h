#ifndef Nodestatements_H_
#define Nodestatements_H_


#include "parser/node/NodeStatement.h"
#include "parser/node/NodeStatements.h"

class NodeStatements: public Node {

		NodeStatement* statement;
		NodeStatements* nextStatements;

	public:
		NodeStatements(void) {
			statement = 0;
			nextStatements = 0;
		}

		virtual ~NodeStatements(void) {
			delete statement;
			delete nextStatements;
		}

		virtual NodeStatement* getStatement() {
			return this->statement;
		}

		virtual NodeStatements* getStatements() {
			return this->nextStatements;
		}

		virtual void addChild(NodeStatement* newStatement) {
			this->statement = newStatement;
		}

		virtual void addChild(NodeStatements* newStatements) {
			this->nextStatements = newStatements;
		}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif /* Nodestatements_H_ */

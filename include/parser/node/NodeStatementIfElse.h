#ifndef NodeStatementIfElse_H_
#define NodeStatementIfElse_H_

#include "parser/node/NodeStatement.h"
#include "parser/node/NodeExp.h"
#include "debug.h"

class NodeStatementIfElse: public NodeStatement {

		NodeExp* expression;
		NodeStatement* ifStatement;
		NodeStatement* elseStatement;

	public:
		NodeStatementIfElse() {
			expression = NULL;
			ifStatement = NULL;
			elseStatement = NULL;
		}

		virtual ~NodeStatementIfElse() {
			delete expression;
			delete ifStatement;
			delete elseStatement;
		}

		virtual NodeExp* getExpression() {
			return expression;
		}

		virtual NodeStatement* getIfStatement() {
			return ifStatement;
		}

		virtual NodeStatement* getElseStatement() {
			return elseStatement;
		}

		virtual void addChild(NodeExp* newExpression) {
			expression = newExpression;
		}

		virtual void addChild(NodeStatement* statement) {
			if(ifStatement == NULL) {
				ifStatement = statement;
			} else if (elseStatement == NULL) {
				elseStatement = statement;
			} else {
				STDERR("Could not add more children to IF_ELSE_STATEMENT!");
			}
		}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif /* NodeStatementIfElse_H_ */

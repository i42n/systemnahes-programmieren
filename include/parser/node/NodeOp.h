#ifndef NodeOp_H_
#define NodeOp_H_


#include "parser/node/NodeLeaf.h"
#include "scanner/Token.h"

class NodeOp : public NodeLeaf {

	public:
		NodeOp(Token* token) {
			// translate token types into OP node types
			switch(token->getType()) {
				case Token::SIGN_PLUS: {
					nodeType = TYPE_OP_PLUS;
					break;
				}
				case Token::SIGN_MINUS: {
					nodeType = TYPE_OP_MINUS;
					break;
				}
				case Token::SIGN_SLASH: {
					nodeType = TYPE_OP_DIVIDE;
					break;
				}
				case Token::SIGN_STAR: {
					nodeType = TYPE_OP_MULTIPLY;
					break;
				}
				case Token::SIGN_SMALLER: {
					nodeType = TYPE_OP_SMALLER;
					break;
				}
				case Token::SIGN_LARGER: {
					nodeType = TYPE_OP_GREATER;
					break;
				}
				case Token::SIGN_DOUBLE_EQUAL: {
					nodeType = TYPE_OP_COMPARE;
					break;
				}
				case Token::SIGN_UNEQUAL: {
					nodeType = TYPE_OP_UNEQUAL;
					break;
				}
				case Token::SIGN_AND: {
					nodeType = TYPE_OP_AND;
					break;
				}
				default: {
					nodeType = TYPE_ERROR;
					// TODO error message here?
				}
			}
		}

		virtual ~NodeOp() {}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif /* NodeOp_H_ */

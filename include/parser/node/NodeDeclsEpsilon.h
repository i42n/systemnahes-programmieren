#ifndef NodeDeclsEpsilon_H_
#define NodeDeclsEpsilon_H_

class NodeDeclsEpsilon: public NodeDecls {

	public:
		NodeDeclsEpsilon() {}
		virtual ~NodeDeclsEpsilon() {}

		virtual void accept(Visitor* visitor) {
			visitor->visit(this);
		}
};

#endif

#ifndef CODE_GENERATOR_H
#define CODE_GENERATOR_H

#include "parser/ParseTree.h"
#include "parser/node/Node.h"
#include "parser/Visitor.h"

#include <cstddef>
#include <iostream>
#include <fstream>

class CodeGenerator : public Visitor {
	public:
		CodeGenerator();
		virtual ~CodeGenerator();
		void makeCode(char* output_filename, ParseTree* tree);

		// these first four can be implemented empty, but they need to be there
		// to enable correct method resolution
		virtual void visit(Node* node);
		virtual void visit(NodeStatement* node);
		virtual void visit(NodeLeaf* node);
		virtual void visit(NodeExp2* node);

		virtual void visit(NodeProg* node);
		virtual void visit(NodeDecls* node);
		virtual void visit(NodeDecl* node);
		virtual void visit(NodeArray* node);
		virtual void visit(NodeStatements* node);
		virtual void visit(NodeStatementAssign* node);
		virtual void visit(NodeStatementPrint* node);
		virtual void visit(NodeStatementRead* node);
		virtual void visit(NodeStatementBlock* node);
		virtual void visit(NodeStatementIfElse* node);
		virtual void visit(NodeStatementWhile* node);

		virtual void visit(NodeExp* node);
		virtual void visit(NodeExp2Exp* node);
		virtual void visit(NodeExp2IdentifierIndex* node);
		virtual void visit(NodeExp2Integer* node);
		virtual void visit(NodeExp2NegativeExp* node);
		virtual void visit(NodeExp2NotExp* node);

		virtual void visit(NodeIndex* node);
		virtual void visit(NodeOpExp* node);
		virtual void visit(NodeOp* node);

		virtual void visit(NodeIdentifier* node);
		virtual void visit(NodeInteger* node);

		virtual void visit(NodeDeclsEpsilon* node);
		virtual void visit(NodeArrayEpsilon* node);
		virtual void visit(NodeStatementsEpsilon* node);
		virtual void visit(NodeOpExpEpsilon* node);
		virtual void visit(NodeIndexEpsilon* node);
		
	private:
		int counter;
		std::ofstream code;
		int getLabelCounter();

};

#endif /* CODE_GENERATOR_H */
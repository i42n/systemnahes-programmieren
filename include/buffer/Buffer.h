#ifndef BUFFER_H_
#define BUFFER_H_

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <pthread.h>

#define DISK_SEGMENT_SIZE 512
#define SINGLE_BUFFER_SIZE (DISK_SEGMENT_SIZE * 10)
#define SEGMENT_COUNT 3
#define BUFFER_SIZE (SINGLE_BUFFER_SIZE * SEGMENT_COUNT)

class Buffer {
	public:
		Buffer(const char* filename);
		virtual ~Buffer();
		char getChar();
		void ungetChar(int how_many_back = 1);
		void writerThreadMethod();

	private:
		int file;
		char* buffer; // buffer mem area
		int offset; // current read char pos in the buffer
		int totalSteps; // total steps moved forward. required to know how far we can go back.

		int next_read_segment;
		
		pthread_t buffer_writer_thread; // writer thread
		pthread_mutex_t read_mutex [SEGMENT_COUNT]; // read mutex for each buffer segment
		pthread_mutex_t write_mutex [SEGMENT_COUNT]; // write mutex for each buffer segment

		void allocateBufferMemory();
		void fillBuffer(int offset); // use read to fill the buffer at current offset
};

#endif /* BUFFER_H_ */

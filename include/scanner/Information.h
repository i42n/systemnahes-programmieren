#ifndef INFORMATION_H_
#define INFORMATION_H_

#include "parser/node/NodeTypeEnum.h"

class Information {
	public:
		Information(char* lexem);
		virtual ~Information();
		bool matchesLexem(const char* other_lexem);
		const char* getLexem() const;

		void setType(NodeType type);
		NodeType getType();
	private:
		char* lexem;
		NodeType nodeType;
};

#endif /* INFORMATION_H_ */

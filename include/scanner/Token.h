#ifndef TOKEN_H
#define TOKEN_H

#include "scanner/Information.h"

class Token {

	public:
		enum Type {
			NEWLINE = 0, // ensure start counting at 0
			SPACE,
			UNKNOWN,
			COMMENT,
			
			IDENTIFIER,
			INTEGER,
			WHILE,
			IF,
			ELSE,
			PRINT,
			READ,
			INT,

			SIGN,
			SIGN_PLUS,
			SIGN_MINUS,
			SIGN_SLASH,
			SIGN_STAR,
			SIGN_SMALLER,
			SIGN_LARGER,
			SIGN_EQUAL,
			SIGN_DOUBLE_EQUAL,
			SIGN_UNEQUAL,
			SIGN_NOT,
			SIGN_AND,
			SIGN_SEMICOLON,

			SQUARE_BRACKET_OPEN, // [
			SQUARE_BRACKET_CLOSE,
			PARANTHESE_OPEN, // (
			PARANTHESE_CLOSE,
			CURLY_BRACKET_OPEN, // {
			CURLY_BRACKET_CLOSE,

			ENUM_END_MARKER // marker to know how many elements the enum contains
		};
		
		// string representation of the Token::Type enum
		static const char* token_type_as_string(Type type) {
			const char* token_type_as_string[ENUM_END_MARKER] = {
				"NEWLINE",
				"SPACE",
				"UNKNOWN",
				"COMMENT",
				
				"IDENTIFIER",
				"INTEGER",
				"WHILE",
				"IF",
				"ELSE",
				"PRINT",
				"READ",
				"INT",

				"SIGN",
				"SIGN_PLUS",
				"SIGN_MINUS",
				"SIGN_SLASH",
				"SIGN_STAR",
				"SIGN_SMALLER",
				"SIGN_LARGER",
				"SIGN_EQUAL",
				"SIGN_DOUBLE_EQUAL",
				"SIGN_UNEQUAL",
				"SIGN_NOT",
				"SIGN_AND",
				"SIGN_SEMICOLON",

				"SQUARE_BRACKET_OPEN", // [
				"SQUARE_BRACKET_CLOSE",
				"PARANTHESE_OPEN", // (
				"PARANTHESE_CLOSE",
				"CURLY_BRACKET_OPEN", // {
				"CURLY_BRACKET_CLOSE"
			};
			return token_type_as_string[type];
		}

		Token(Type type, int column, int line, Information* information, long int value);
		virtual ~Token();
		
		Information* getInformation() const;
		Type getType() const;
		int getColumn() const;
		int getLine() const;
		long int getValue() const;


	private:
		Information* information;
		int column;
		int line;
		long int value;
		Type type;
};

#endif /* TOKEN_H */

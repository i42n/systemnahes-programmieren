#ifndef SCANNER_H_
#define SCANNER_H_

#define SCANNER_BUFFER_SIZE 4096

#include "buffer/Buffer.h"
#include "automat/Automat.h"
#include "scanner/Information.h"
#include "scanner/Token.h"
#include "symboltable/SymbolTable.h"
#include "symboltable/SymbolTableEntry.h"

class Scanner {
	public:
		Scanner(const char* filename);
		virtual ~Scanner();
		Token* nextToken();
		void freeToken(Token* token);
	private:
		char scanner_buffer [SCANNER_BUFFER_SIZE];
		int char_count;

		Buffer* buffer;
		Automat* automat;
		SymbolTable* symboltable;

		void resetForNextLexem(int bad_char_count);
};

#endif /* SCANNER_H_ */

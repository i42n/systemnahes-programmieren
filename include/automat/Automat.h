#ifndef AUTOMAT_H
#define AUTOMAT_H

#include "scanner/Token.h"

class Automat {
	private:
		typedef enum {
			START = 0,
			IDENTIFIER,
			SPACE,
			INTEGER,
			ANYSIGN,
			SIGN2,
			SIGN3,
			SIGN4,
			SIGN5,
			COMMENT1,
			COMMENT2,
			COMMENT3,
			NEWLINE,
			UNDEFINED
		} State;

		State currentState;
		State lastFinalState;

		int startColumn;
		int startLine;
		int currentLine;
		int currentColumn;
		int back;

		void set_last_final_state(State);

	public:
		Automat();
		virtual ~Automat();

		bool putChar (char c);
		void reset(int bad_char_count);

		bool isSpace(char c);
		bool isIdentifier(char c);
		bool isInteger(char c);
		bool isSign(char c);
		bool isNewline(char c);

		Token::Type getType() const;
		int getBack() const;
		int getLine() const;
		int getColumn() const;
		
		// Debug
		int debugLine();
		int debugColumn();
};

#endif /* AUTOMAT_H */

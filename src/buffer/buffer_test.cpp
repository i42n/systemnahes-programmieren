#include "buffer/Buffer.h"

#include <iostream>

int main(int argc, char **argv) {

	if(argc == 2) {	
		Buffer* buffer = new Buffer(argv[1]);

		// print the complete file
		char c;
		long int counter = 0;
		while((c = buffer->getChar()) != -1) {
			std::cout << c;
			counter++;
		}

		std::cout << std::endl;
		std::cout << std::endl;

		std::cout << "counted chars: " << counter << std::endl;

		delete buffer;
	}

}

#include "buffer/Buffer.h"

#include "debug.h"

/**
 * thread initialization method. it calls the object method to
 * have access to all the object attributes
 * @param data pointer to the Buffer object
 */
void* writerThreadStart(void* data) {
	Buffer* buffer = static_cast<Buffer*> (data);
	buffer->writerThreadMethod();
	pthread_exit(0);
}

/**
 * create a new buffer with filename
 */
Buffer::Buffer(const char* filename) {

	// open the file with O_DIRECT flag
	if( (file = open(filename, O_RDONLY | O_DIRECT)) == -1) {
		STDERR("opening file failed");
	}

	offset = 0;
	next_read_segment = 0;
	totalSteps = 0;

	allocateBufferMemory();

	// initialize mutexes
	for(int i = 0; i < SEGMENT_COUNT; i++) {
		pthread_mutex_init(&read_mutex[i], NULL);
		pthread_mutex_lock(&read_mutex[i]);
		pthread_mutex_init(&write_mutex[i], NULL);
	}

	// create writer thread
	if(pthread_create(&buffer_writer_thread, NULL, writerThreadStart, this)) {
		// thread creation failed, return value is not 0
		STDERR("Could not create BufferWriter thread.");
	}
}

Buffer::~Buffer() {
	close(file);
	free(buffer);
}

/**
 * buffer writer thread which reads segments from disk and fills the buffer
 * with content.
 * it works as endless loop going through the buffer segments and filling them.
 * before writing to a segment it has to acquire the write lock. the write lock
 * will only be released by the reader thread if it got far enough and needs new data.
 */
void Buffer::writerThreadMethod() {
	int current_buffer = 0;
	DEBUG_STDOUT("starting writer thread");
	while(true) {
		pthread_mutex_lock(&write_mutex[current_buffer]);
		DEBUG_STDOUT("locked write mutex " << current_buffer);
		DEBUG_STDOUT("filling segment " << current_buffer);
		fillBuffer(SINGLE_BUFFER_SIZE * current_buffer);
		pthread_mutex_unlock(&read_mutex[current_buffer]);
		DEBUG_STDOUT("released read mutex " << current_buffer);
		current_buffer = (current_buffer + 1) % SEGMENT_COUNT;
	}
}

/**
 * returns the next char from the buffer
 * returns 0 if the end of the file is reached and there are no more chars in the buffer
 *
 * this works as reader thread in a way, because it has to acquire the read lock
 * for each buffer segment it wants to read from.
 * @return the next char in the buffer
 */
char Buffer::getChar(){
	// save current char as offset will be modified
	if(offset < 0 || offset > (BUFFER_SIZE - 1))
		STDERR("Buffer failed. Offset out of range! Current offset is " << offset);
	
	// calc the current buffer segment from offset
	int buffer_segment = offset / SINGLE_BUFFER_SIZE;
	// check if we are at the start of a new segment. if this is the case we have to acquire the read lock for this segment
	if(offset % SINGLE_BUFFER_SIZE == 0 && buffer_segment == next_read_segment) {
		// we are at the start of a new segment
		// therefore we can only continue to read chars if the read lock mutex is free
		DEBUG_STDOUT("request read access to segment " << buffer_segment);
		pthread_mutex_lock(&read_mutex[buffer_segment]);
		DEBUG_STDOUT("got read access to segment " << buffer_segment);

		// additionally the write lock two segments back has to be released
		int release_segment = buffer_segment - 2;
		if (release_segment < 0)
			release_segment = SEGMENT_COUNT + release_segment; // add here because release_segment is already negative
		pthread_mutex_unlock(&write_mutex[release_segment]);
		DEBUG_STDOUT("release write lock " << release_segment);

		// update next segment variable
		next_read_segment = (next_read_segment + 1) % SEGMENT_COUNT;
	}

	// actually read the next char from the buffer
	char returnChar = buffer[offset];

	// update offset 
	if(returnChar != -1) // only step further if the end of the file is not reached
		offset++;
	offset = offset % BUFFER_SIZE; // set back to 0 if larger than BUFFER_SIZE

	// count total steps done to know how far we can go back if only the first
	// chars have been accessed
	totalSteps++;
	return returnChar;
}


/**
 * go back in the buffer. default is one back.
 * if the buffer can not go back that far, the method will do nothing but print an error
 * @param how_many_back set how many chars the buffer should go back
 */
void Buffer::ungetChar(int how_many_back){
	// calc theoretical max steps to go back
	int theoretical_max_back = SINGLE_BUFFER_SIZE + offset % SINGLE_BUFFER_SIZE;
	// choose between theoretical max back and total steps done. the smallest is more important
	int max_back = theoretical_max_back < totalSteps ? theoretical_max_back : totalSteps;
	if(how_many_back <= max_back && how_many_back >= 0){
		// we can actually go back that far
		// move the offset back
		offset -= how_many_back;
		// if we went past the start of the buffer, go back the rest from the end of the buffer
		if(offset < 0) {
			offset = BUFFER_SIZE + offset; // offset is already negative!
		}
	} else {
		STDERR("you can not go back that far: " << how_many_back);
	}
}

/**
 * allocate BUFFER_SIZE memory and align it to DISK_SEGMENT_SIZE.
 * alignment is required to use O_DIRECT flag
 */
void Buffer::allocateBufferMemory() {
	void* mem; // void pointer required for memalign command
	// allocate memory alligned for DISK_SEGMENT_SIZE
	if ((posix_memalign(&mem, DISK_SEGMENT_SIZE, sizeof(char) * BUFFER_SIZE)) != 0) {
		STDERR("aligning memory failed");
	}
	buffer = (char*) mem;
}

/**
 * fill the buffer with new data from the file.
 * if less than requested bytes could be read, this method marks the end of the file
 * in the buffer by writing a 0 into the buffer.
 * @param offset the offset to the start of the buffer marking the start of the buffer segment to fill with new data 
 */
void Buffer::fillBuffer(int offset) {
	int bytes_read = read(file, buffer + offset, SINGLE_BUFFER_SIZE);
	// if less than requested bytes could be read, the end of the file is reached
	// mark the end by writing a 0 into the buffer, so getChar will return the 0
	if(bytes_read < SINGLE_BUFFER_SIZE)
		buffer[offset + bytes_read] = -1;
}
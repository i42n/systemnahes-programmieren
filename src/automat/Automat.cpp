#include "automat/Automat.h"
#include "scanner/Token.h"
#include "debug.h"

static const char signs [] = {'+', '-', '/', '*', '<', '>', '=', '!', '&', ';', '(', ')', '{', '}', '[', ']' };

Automat::Automat() {
	currentState = START;
	startColumn = 1;
	startLine = 1;
	currentLine = 1;
	currentColumn = 1;
	back = 0;
}

Automat::~Automat() {
	// nothing to to here
}

/**
 * CHECKS FOR THE STATES OF THE MACHIN
 */

bool Automat::isSpace(char c) {
	return c == ' ' || c == '\t';
}

bool Automat::isIdentifier(char c) {
	return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9' && currentState == IDENTIFIER));
}

bool Automat::isInteger(char c) {
	return c >= '0' && c <= '9';
}

bool Automat::isSign(char c) {
	for(int i = 0; i < 16; i++) {
		if(signs[i] == c)
			return true;
	}
	return false;
}

bool Automat::isNewline(char c) {
	return c == '\n' || c == '\r';
}

/**
 * insert next char into the machine to change its state.
 * if this returns false, you can check the last_final_state and back via getters.
 * afterwards do not forget to call reset() before inserting new chars
 * @param  c the char to insert
 * @return   returns false if it is not possible that further insertions will lead to a valid state.
 */
bool Automat::putChar(char c) {
	currentColumn++;
	switch(currentState) {

		/**
		 * #################
		 * START STATE
		 * #################
		 */
		case START: {
			if (isIdentifier(c)) {
				currentState = IDENTIFIER;
				set_last_final_state(IDENTIFIER);

				DEBUG_STDOUT("Switching to IdentifierState");

				return true;
			} else if (isSpace(c)) {
				/** SPACE **/
				currentState = SPACE;
				set_last_final_state(SPACE);

				DEBUG_STDOUT("Switching to SpaceState");

				return true;

			} else if (isInteger(c)) {
				/** INTEGER **/
				currentState = INTEGER;
				set_last_final_state(INTEGER);
				
				DEBUG_STDOUT("Switching to IntegerState");

				return true;

			} else if (isSign(c) && c != '/' && c != '=') {
				/** ANYSIGN **/
				currentState = ANYSIGN;
				set_last_final_state(ANYSIGN);

				DEBUG_STDOUT("Switching to AnySignState");

				return true;

			} else if (c == '/') {
				/**SIGN 2 **/
				currentState = SIGN2;
				set_last_final_state(SIGN2);

				DEBUG_STDOUT("Switching to SignState 2");

				return true;

			} else if (c == '=') {
				/**SIGN 3 **/
				currentState = SIGN3;
				set_last_final_state(SIGN3);

				DEBUG_STDOUT("Switching to SignState 3");

				return true;

			} else if (isNewline(c)){
				/** NEWLINE **/
				currentState = NEWLINE;
				set_last_final_state(NEWLINE);
				currentLine++;
				currentColumn = 1;

				DEBUG_STDOUT("Switching to NewlineState");
				
				return true;

			} else {
				back++;
				return false;
			}
		}

		/**
		 * #################
		 * IDENTIFIER STATE
		 * #################
		 */
		case IDENTIFIER: {
			if (isIdentifier(c)) {
				
				DEBUG_STDOUT("Staying in IdentifierState");
				
				return true;
			} else {
				back++;
				return false;
			}
		}

		/**
		 * #################
		 * SPACE STATE
		 * #################
		 */
		case SPACE: {
			if (isSpace(c)) {

				DEBUG_STDOUT("Staying in SpaceState");

				return true;
			} else {
				back++;
				return false;
			}
		}
		
		/**
		 * #################
		 * INTEGER STATE
		 * #################
		 */
		case INTEGER: {
			if (isInteger(c)) {
				DEBUG_STDOUT("Staying in IntegerState");

				return true;
			} else {
				back++;
				return false;
			}
		}

		/**
		 * #################
		 * NEWLINE STATE
		 * #################
		 */
		case NEWLINE: {
			if (isNewline(c)) {
				currentColumn = 1;
				currentLine++;

				DEBUG_STDOUT("Staying in NewlineState");
				
				return true;

			} else {
				back++;
				return false;
			}
		}

		/**
		 * #################
		 * SIGN2 STATE
		 * #################
		 */
		case SIGN2: {
			if (c == '*') {
				currentState = COMMENT1;
				set_last_final_state(COMMENT1);
				DEBUG_STDOUT("Switching to CommentState 1");

				return true;
			} else {
				back++;
				return false;
			}
		}

		/**
		 * #################
		 * SIGN3 STATE
		 * #################
		 */
		case SIGN3: {
			if (c == '!') {
				currentState = SIGN4;
				back++;
				DEBUG_STDOUT("Switching to SignState 4");

				return true;

			} else if (c == '=') {
				currentState = SIGN5;
				set_last_final_state(SIGN5);

				DEBUG_STDOUT("Switching to SignState 5");

				return true;

			} else {
				back++;
				return false;
			}
		}
		
		/**
		 * #################
		 * SIGN4 STATE
		 * #################
		 */
		case SIGN4: {
			if (c == '=') {
				currentState = SIGN5;
				set_last_final_state(SIGN5);

				DEBUG_STDOUT("Switching to SignState 5");

				return true;
			} else {
				back++;
				return false;
			}
		}

		
		/**
		 * #################
		 * COMMENT1 STATE
		 * #################
		 */
		case COMMENT1: {
			if (c == '*'){
				currentState = COMMENT2;
				set_last_final_state(COMMENT2);
				DEBUG_STDOUT("Switching to CommentState 2");
				
				return true;
			} else if(isNewline(c)) {
				currentColumn = 1;
				currentLine++;
				DEBUG_STDOUT("Staying in CommentState 1");
				return true;
			} else {
				DEBUG_STDOUT("Staying in CommentState 1");
				
				return true;		
			}
		}
		/**
		 * #################
		 * COMMENT2 STATE
		 * #################
		 */
		case COMMENT2: {
			if (c == '/'){
				currentState = COMMENT3;
				set_last_final_state(COMMENT3);
				DEBUG_STDOUT("Switching to CommentState 3");
				
				return true;	

			} else if (c == '*'){
				DEBUG_STDOUT("Staying in CommentState 2");
				
				return true;

			} else if (isNewline(c)){
				currentColumn = 1;
				currentLine++;
				DEBUG_STDOUT("Staying in CommentState 2");
				return true;
			} else {
				currentState = COMMENT1;
				set_last_final_state(COMMENT1);
				DEBUG_STDOUT("Switching to CommentState 1");
				
				return true;
			}
		}

		/**
		 * #################
		 * COMMENT3 STATE
		 * #################
		 */
		
		/**
		 * #################
		 * ANYSIGN STATE
		 * #################
		 */
		
		/**
		 * #################
		 * SIGN5 STATE
		 * #################
		 */
		
		default: {
			back++;
			return false;
		}
	}
}

/**
 * translate final states of the machine into token types
 * @return last final state of the machine as token type. if there is no valid last final state, this will 
 * return the token type UNKNOWN
 */
Token::Type Automat::getType() const {
	switch(lastFinalState) {
		case SPACE:
			return Token::SPACE;
		case IDENTIFIER:
			return Token::IDENTIFIER;
		case NEWLINE:
			return Token::NEWLINE;
		case INTEGER:
			return Token::INTEGER;
		case ANYSIGN:
			return Token::SIGN;
		case SIGN2:
			return Token::SIGN_SLASH;
		case SIGN3:
			return Token::SIGN_EQUAL;
		case SIGN5:
			return Token::SIGN;
		case COMMENT1:
			return Token::COMMENT;
		case COMMENT2:
			return Token::COMMENT;
		case COMMENT3:
			return Token::COMMENT;
		default:
			return Token::UNKNOWN;
	}
}

/**
 * how many chars were invalid after the last final state
 * @return number of invalid chars after the last final state
 */
int Automat::getBack() const {
	return back;
}

/**
 * get the start line of the last recognized lexem
 * @return [description]
 */
int Automat::getLine() const {
	return startLine;
}

/**
 * get the column of the last recognized lexem
 * @return [description]
 */
int Automat::getColumn()  const{
	return startColumn;
}

/**
 * reset the machine to start processing the next lexem
 * @param bad_char_count how many chars back
 */
void Automat::reset(int bad_char_count) {
	// we go back in the buffer, so do not count these chars
	currentColumn -= bad_char_count;
	back = 0;
	lastFinalState = UNDEFINED;
	currentState = START;
	startLine = currentLine;
	startColumn = currentColumn;
}

/**
 * set last final state. this is a separate method as it is easy to forget resetting back = 0
 * @param state the last final state
 */
void Automat::set_last_final_state(State state) {
	lastFinalState = state;
	back = 0;
}


/* ######################
*  Debug Rückgabemethoden
*  ######################
*/

int Automat::debugLine(){
	return currentLine;
}

int Automat::debugColumn(){
	return currentColumn;
}
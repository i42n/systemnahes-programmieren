#include <iostream>
#include "automat/Automat.h"
#include "scanner/Token.h"
#include <string>
#include <cstdlib>
#include <cstring>

// Zufällige Zeichen erzeugen
static const char alphanum[] =
"0123456789"
//"!@#$%^&*"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz ";

void genRandom(char* string, int length) {
	for(int i = 0; i < length; i++){
    	string[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }
}


// Ausgabemethode
void checkParameter(Automat& automat){
	std::cout << "Der Typ ist: ";
	std::cout << Token::token_type_as_string(automat.getType()) << std::endl;
	
	std::cout << "Back ist: ";
	std::cout << automat.getBack() << std::endl;
	
	automat.reset(1);
	
	std::cout << "Column ist: ";
	std::cout << automat.getColumn() << std::endl;
	
	std::cout << "Line ist: ";
	std::cout << automat.getLine() << std::endl;
	
	std::cout << std::endl;
}

/*
* ##################
* Testmethoden
* ##################
*/

void IdentifierTesten(){
	Automat automat;

	automat.putChar('a');
	automat.putChar('b');
	automat.putChar('c');
	automat.putChar('D');
	automat.putChar('e');
	automat.putChar('f');
	automat.putChar('G');
	automat.putChar('H');
	automat.putChar('i');
	automat.putChar('j');
	
	// abbrechen
	automat.putChar('.');
	
	checkParameter(automat);
}

void IntegerTesten(){
	Automat automat;
	
	automat.putChar('1');
	automat.putChar('2');
	automat.putChar('3');
	automat.putChar('4');
	automat.putChar('5');
	automat.putChar('6');
	automat.putChar('7');
	automat.putChar('8');
	automat.putChar('9');
	automat.putChar('1');
	automat.putChar('1');
	
	// abbrechen
	automat.putChar('h');
	
	checkParameter(automat);
}

void KommentarTesten(){
	Automat automat;
	
	automat.putChar('/');
	automat.putChar('*');
	automat.putChar('K');
	automat.putChar('o');
	automat.putChar('m');
	automat.putChar('m');
	automat.putChar('e');
	automat.putChar('*');
	automat.putChar('n');
	automat.putChar('t');
	automat.putChar('a');
	automat.putChar('r');
	automat.putChar('2');
	automat.putChar('a');
	automat.putChar('1');
	automat.putChar('*');
	automat.putChar('/');
	
	// abbrechen
	automat.putChar('p');
	
	checkParameter(automat);
}

void LeerraumTesten(){
	Automat automat;
	
	automat.putChar(' ');
	automat.putChar(' ');
	automat.putChar(' ');
	automat.putChar(' ');
	automat.putChar(' ');
	automat.putChar(' ');
	
	// abbrechen
	automat.putChar('3');
	
	checkParameter(automat);
}

void NewLineTesten(){
	Automat automat;
	
	automat.putChar('\n');
	automat.putChar('\n');
	automat.putChar('\n');
	automat.putChar('\n');
	automat.putChar('\n');
	automat.putChar('\n');
	automat.putChar('\n');
	automat.putChar('\n');
	
	// abbrechen
	automat.putChar(';');
	
	checkParameter(automat);
}

void UngleichTesten(){
	Automat automat;
	
	automat.putChar('=');
	automat.putChar('!');
	automat.putChar('=');
	
	// abbrechen
	automat.putChar('b');
	
	checkParameter(automat);
}

void SignTesten(){
	Automat automat;
	
	automat.putChar(':');
	automat.putChar(';');
	automat.putChar('.');
	automat.putChar('<');
	
	// abbrechen
	automat.putChar('3');
	
	checkParameter(automat);
}

void Zeichentester(){
	Automat automat;
	
	// 500.000 random Zeichen
	char random_zeichen [500000];
	genRandom(random_zeichen, 500000);
	
	
	// 500.000 newlines
	char newlines [500000];
	
	for(size_t i = 0; i < 500000; ++i)
	{
		newlines[i] = '\n';
	}
	
	
	// 250.000 Zeichen und 250.000 Newlines
	char cunl [500000];
	genRandom(cunl, 250000);
	
	for(size_t i = 250000; i < 500000; ++i)
	{
		cunl[i] = '\n';
	}
	
	//std::cout << cunl << std::endl;
	
	// +++++++++++++++++++++++++++++++
	// Den String in den Automat geben
	for(size_t i = 0; i < 250000; ++i)
	{
		automat.putChar(cunl[i]);
	}
	
	checkParameter(automat);
	
	automat.reset(0);
	
	for(size_t i = 250000; i < 500000; ++i)
	{
		automat.putChar(cunl[i]);
	}
	
	checkParameter(automat);
}


int main (int argc, char* argv[]){
	
	 Zeichentester();
	
	//IdentifierTesten();
	// 
	// IntegerTesten();
	//  
	// KommentarTesten();
	//  
	// LeerraumTesten();
	//  
	// NewLineTesten();
	//  
	// UngleichTesten();
	// 
	// SignTesten();
}

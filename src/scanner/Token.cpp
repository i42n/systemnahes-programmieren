#include "scanner/Token.h"

Token::Token(Token::Type type, int column, int line, Information* information, long int value) {
	this->type = type;
	this->column = column;
	this->line = line;
	this->information = information;
	this->value = value;
}

Token::~Token() {
	// do not delete information object here, because there might be another
	// reference in another token
}

Information* Token::getInformation() const {
	return information;
}

Token::Type Token::getType() const {
	return type;
}

int Token::getColumn() const {
	return column;
}

int Token::getLine() const {
	return line;
}

long int Token::getValue() const {
	return value;
}
#include <iostream>
#include <stdlib.h>
#include <errno.h>

#include "scanner/Scanner.h"
#include "debug.h"

Scanner::Scanner(const char* filename) {
	buffer = new Buffer(filename);
	automat = new Automat();
	symboltable  = new SymbolTable();
}

Scanner::~Scanner() {
	delete buffer;
	delete automat;
	delete symboltable;
}

/**
 * continues to parse the file and creates a token for the next
 * found lexem.
 * returns NULL if no new lexem could be created, because the end of the file was reached.
 * @return A wonderful token
 */
Token* Scanner::nextToken() {
	char c;
	do {
		c = buffer->getChar();
		if(c == -1) {
			// can not get more chars
			break;
		}
		if(char_count < SCANNER_BUFFER_SIZE) {
			// buffer the char in local buffer
			scanner_buffer[char_count++] = c;
		} else {
			// more chars to buffer than the scanner buffer can handle
			STDERR("Lexem larger than " << SCANNER_BUFFER_SIZE << " characters. The scanner buffer is not large enough to handle that.");
		}
	} while(automat->putChar(c));

	if(char_count == 0) {
		// if the buffer did not return any new chars, we can not build more tokens
		return NULL;
	}

	// otherwise at least 1 char could be read and put into the machine
	// so we can analyse what we got now...

	Token::Type lexem_type = automat->getType();
	int bad_char_count = automat->getBack();
	
	if(lexem_type == Token::NEWLINE || lexem_type == Token::SPACE) {
		// do not create tokens for newline or space type
		// ignore theses token types
		DEBUG_STDOUT("SPACE OR NEWLINE TOKEN: Ignoring!");
		resetForNextLexem(bad_char_count);
		
		return nextToken();
	} else if (lexem_type == Token::UNKNOWN) {
		scanner_buffer[char_count] = '\0';
		STDMESSAGE("UNKNOWN TOKEN: " << scanner_buffer);

		// reset 0 chars as they are unknown. if we read them again
		// this will result in a endless loop!
		resetForNextLexem(0);
		
		return nextToken();
	} else if (lexem_type == Token::COMMENT) {
		// do not create a token for a comment
		// treat it just as whitespace
		scanner_buffer[char_count - bad_char_count] = '\0';
		DEBUG_STDOUT("COMMENT TOKEN: " << scanner_buffer);
		resetForNextLexem(bad_char_count);
		
		return nextToken();
	} else if (lexem_type == Token::INTEGER) {
		// create Integer Token
		int column = automat->getColumn();
		int line = automat->getLine();

		// parse string to integer
		scanner_buffer[char_count - bad_char_count] = '\0';
		long int value = strtol(scanner_buffer, NULL, 10);

		resetForNextLexem(bad_char_count);

		// this might fail
		if(errno == EINVAL) {
			STDMESSAGE("Error processing integer value.");
		} else if(errno == ERANGE) {
			STDMESSAGE("Integer out of range for long int.");
		} else {
			Token* token = new Token(lexem_type, column, line, NULL, value);
			return token;
		}

		// if this could not be parsed, continue with the next token.
		return nextToken();
	} else {
		int column = automat->getColumn();
		int line = automat->getLine();

		int lexem_length = char_count - bad_char_count;
		scanner_buffer[char_count - bad_char_count] = '\0';
		SymbolTableEntry* entry = symboltable->addLexem(scanner_buffer, lexem_length, lexem_type);
		Token* token = new Token(entry->getType(), column, line, entry->getInformation(), 0);

		resetForNextLexem(bad_char_count);

		return token;
	}

}

void Scanner::resetForNextLexem(int bad_char_count) {
	buffer->ungetChar(bad_char_count);
	automat->reset(bad_char_count);
	char_count = 0;
}

/**
 * encapsulate the free process for tokens as the user of scanner does not know
 * all the additional ressources that were allocated during the creation of the token.
 * @param  token [description]
 * @return       [description]
 */
void Scanner::freeToken(Token* token) {
	delete token;
}

#include "scanner/Scanner.h"
#include "scanner/Token.h"

#include <cstddef>
#include <iostream>
#include <fstream>

int main(int argc, char **argv) {

	// check if a path to a text file is provided
	if(argc == 3) {	
		Scanner* scanner = new Scanner(argv[1]);
		Token* token = scanner->nextToken();

		std::ofstream output_file;
		output_file.open (argv[2]);

		while(token != NULL) {
			output_file << "Token: " << Token::token_type_as_string(token->getType()) << '\t';
			output_file << "Line: " << token->getLine() << ' ';
			output_file << "Column: " << token->getColumn() << ' ';
			if(token->getType() == Token::INTEGER) {
				output_file << "Value: " << token->getValue() << '\n';			
			} else {
				output_file << "Lexem: " << token->getInformation()->getLexem() << '\n';			
			}

			token = scanner->nextToken();
		}

		output_file.close();
		delete scanner;
	} else {
		std::cout << "Please call the test with a path to a test file and a path to the output file!\nExample: 'scanner_test test.txt output.txt'" << std::endl;
	}

}


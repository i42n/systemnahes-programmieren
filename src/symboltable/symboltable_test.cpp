#include "symboltable/SymbolTable.h"
#include "symboltable/SymbolTableEntry.h"
#include "scanner/Information.h"
#include "scanner/Token.h"

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstring>

static const char alphanum[] =
"0123456789"
"!@#$%^&*"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz";

void genRandom(char* string, int length) {
	for(int i = 0; i < length; i++){
    	string[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }
}

int main(int argc, char **argv) {

	SymbolTable* symboltable = new SymbolTable();

	Information* info = symboltable->addLexem("lexem", 5, Token::IDENTIFIER)->getInformation();
	std::cout << info->getLexem() << std::endl;
	std::cout << info->matchesLexem("lexem") << std::endl;
	std::cout << info->matchesLexem("foobar") << std::endl;

	Information* info2 = symboltable->addLexem("lexem", 5, Token::IDENTIFIER)->getInformation();
	std::cout << info << "\n" << info2 << std::endl;

	for(int i = 0; i < 10000; i++) {
		char random_string [30];
		genRandom(random_string, 30);
		Information* info = symboltable->addLexem(random_string, 30, Token::IDENTIFIER)->getInformation();
		std::cout << info->getLexem() << std::endl;
	}

	delete symboltable;


}

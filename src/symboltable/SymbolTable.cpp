#include <cstddef> // for NULL

#include "symboltable/SymbolTable.h"
#include "symboltable/SymbolTableEntry.h"

#include "debug.h"

SymbolTable::SymbolTable() {
	stringTable = new StringTable();
	// clear all entries and fill them with NULL
	for(int index = 0; index < TABLE_SIZE; index++) {
		table[index] = NULL;
	}
	// insert special symbols
	initSymbols();
}

SymbolTable::~SymbolTable() {
	delete stringTable;
	// delete all SymbolTableEntries
	SymbolTableEntry* current_entry;
	for(int index = 0; index < TABLE_SIZE; index++) {
		current_entry = table[index];
		while(current_entry != NULL) {
			SymbolTableEntry* to_delete = current_entry;
			current_entry = current_entry->getNext();
			delete to_delete;
		}
	}
}

/**
 * put some lexems into the symbol table to identify them easier
 */
void SymbolTable::initSymbols() {
	// special identifiers
	addLexem("while", 5, Token::WHILE); addLexem("WHILE", 5, Token::WHILE);
	addLexem("if", 2, Token::IF); addLexem("IF", 2, Token::IF);
	addLexem("print", 5, Token::PRINT);
	addLexem("read", 4, Token::READ);
	addLexem("else", 4, Token::ELSE); addLexem("ELSE", 4, Token::ELSE);
	addLexem("int", 3, Token::INT);

	// signs
	// slash and equal are processed by the state machine
	addLexem("+", 1, Token::SIGN_PLUS);
	addLexem("-", 1, Token::SIGN_MINUS);
	//addLexem("/", 1, Token::SIGN_SLASH);
	addLexem("*", 1, Token::SIGN_STAR);
	addLexem("<", 1, Token::SIGN_SMALLER);
	addLexem(">", 1, Token::SIGN_LARGER);
	//addLexem("=", 1, Token::SIGN_EQUAL);
	addLexem("==", 2, Token::SIGN_DOUBLE_EQUAL);
	addLexem("=!=", 3, Token::SIGN_UNEQUAL);
	addLexem("!", 1, Token::SIGN_NOT);
	addLexem("&", 1, Token::SIGN_AND);
	addLexem(";", 1, Token::SIGN_SEMICOLON);

	// brackets
	addLexem("[", 1, Token::SQUARE_BRACKET_OPEN);
	addLexem("]", 1, Token::SQUARE_BRACKET_CLOSE);
	addLexem("(", 1, Token::PARANTHESE_OPEN);
	addLexem(")", 1, Token::PARANTHESE_CLOSE);
	addLexem("{", 1, Token::CURLY_BRACKET_OPEN);
	addLexem("}", 1, Token::CURLY_BRACKET_CLOSE);
}

/**
 * create new SymbolTableEntry. This is only in its own method for code deduplication.
 * @param  lexem        the lexem to put into the string table / information object
 * @param  lexem_length the length of the lexem without the \0 at the end
 * @param  token_type the type of the token the machine recognized
 * @return              the complete symbol table entry with its information object containing the pointer to the lexem in the string table
 */
SymbolTableEntry* SymbolTable::create_new_entry(const char* lexem, int lexem_length, Token::Type token_type) {
	char* string_table_pointer = stringTable->insert(lexem, lexem_length);
	Information* new_entry_info = new Information(string_table_pointer);
	return new SymbolTableEntry(token_type, new_entry_info);
}

/**
 * add a new lexem into the symbol table.
 * if the given lexem is already in the symbol table the corresponding Information object will be returned.
 * @param  lexem        the lexem to insert
 * @param  lexem_length the length of the lexem. without the \0 at the end!
 * @param  token_type the type of the token the machine recognized
 * @return              Information object that contains the lexem
 */
SymbolTableEntry* SymbolTable::addLexem(const char* lexem, int lexem_length, Token::Type token_type) {
	int index = hash(lexem, 0) % TABLE_SIZE;
	if(table[index] == NULL) {
		// no existing entry at this index
		SymbolTableEntry* new_entry = create_new_entry(lexem, lexem_length, token_type);
		table[index] = new_entry;
		return new_entry;
	} else {
		// there already is an entry at this index
		// walk through all the entries at this index and check if one of them matches the given lexem
		SymbolTableEntry* current_entry = table[index];
		while(current_entry != NULL && !current_entry->getInformation()->matchesLexem(lexem) ) {
			current_entry = current_entry->getNext();
		}
		if(current_entry != NULL) {
			// we found a matching lexem
			return current_entry;
		} else {
			// no matching lexem found
			// insert new entry at the end of the list
			SymbolTableEntry* current_entry = table[index];
			while(current_entry->getNext() != NULL) {
				current_entry = current_entry->getNext();
			}
			SymbolTableEntry* new_entry = create_new_entry(lexem, lexem_length, token_type);
			current_entry->setNext(new_entry);
			return new_entry;
		}
	}
}

/**
 * Calulates a hash of a string.
 * http://stackoverflow.com/questions/98153/whats-the-best-hashing-algorithm-to-use-on-a-stl-string-when-using-hash-map
 * @param  string string to calc hash of
 * @param  seed   seed. default is 0
 * @return        hash value
 */
unsigned int SymbolTable::hash ( const char* string, unsigned int seed = 0) {
	unsigned hash = seed;
	while (*string) {
		hash = hash * 101 + *string++;
	}
	return hash;
}
#include "symboltable/StringTable.h"

#include <stdlib.h>
#include <string.h> // for memcpy

#include "debug.h"

StringTable::StringTable() {
	first_entry = (StringTableEntry *) malloc(sizeof(StringTableEntry));
	first_entry->space_left = ALLOC_SIZE;
	current_entry = first_entry;
	nextFreeSpace = first_entry->data;
}

StringTable::~StringTable() {
	current_entry = first_entry;
	StringTableEntry* next_entry;
	// remove the string table entries step by step
	while(current_entry != NULL) {
		next_entry = current_entry->next;
		free(current_entry);
		current_entry = next_entry;
	}
}

/**
 * Insert a new lexem into the string table. This method copies the given lexem.
 * @param  lexem Pointer to the lexem string to insert
 * @param  lexem_length  Lenght of the lexem to insert (without the \0 as string termination!)
 * @return Returns a pointer to the start of lexem in the string table. Each lexem is terminated by \0. Returns NULL if lexem can not be stored.
 */
char* StringTable::insert(const char* lexem, int lexem_length){
	// check if there is enough space left
	if (lexem_length + 1 < current_entry->space_left){
		// if there is enough space left, copy the lexem
		memcpy(nextFreeSpace, lexem, lexem_length);
		// set nullpointer at the end of the lexem
		nextFreeSpace[lexem_length] = '\0';
		char* lexem_pointer = nextFreeSpace;
		// update nextFreeSpace pointer
		nextFreeSpace += lexem_length + 1;
		// update free space counter
		current_entry->space_left -= lexem_length + 1;
		return lexem_pointer;
	} else if (lexem_length < ALLOC_SIZE){
		// there is not enough space left, but we can allocate more
		increaseSize();
		return insert(lexem, lexem_length);
	} else {
		STDERR("The lexem you try to insert into the string table is larger than the string table buffer size. Can not insert it!");
	}
}

/**
 * allocates a new string table entry and sets it as current entry.
 */
void StringTable::increaseSize() {
	// allocate new entry
	StringTableEntry* new_entry = (StringTableEntry *) malloc(sizeof(StringTableEntry));
	// initialize the space_left value
	new_entry->space_left = ALLOC_SIZE;
	// set next pointer for current entry
	current_entry->next = new_entry;
	// set new allocated entry as current entry
	current_entry = new_entry;
	// update next free space pointer
	nextFreeSpace = new_entry->data;
}
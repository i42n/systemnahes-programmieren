#include "symboltable/StringTable.h"

#include <iostream>

int main(int argc, char **argv) {

	StringTable* string_table = new StringTable();

	for(int i = 0; i < 500; i++) {
		const char* inserted_string = string_table->insert("test", 4);
		std::cout << inserted_string << std::endl;
	}

	delete string_table;

}

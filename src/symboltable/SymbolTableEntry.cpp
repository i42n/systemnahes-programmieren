#include <cstddef> // for NULL

#include "symboltable/SymbolTableEntry.h"

SymbolTableEntry::SymbolTableEntry(Token::Type token_type, Information* information) {
	this->information = information;
	this->token_type = token_type;
	next = NULL;
}

SymbolTableEntry::~SymbolTableEntry() {
	// TODO
}

SymbolTableEntry* SymbolTableEntry::getNext() const {
	return next;
}

Token::Type SymbolTableEntry::getType() const {
	return token_type;
}

void SymbolTableEntry::setNext(SymbolTableEntry* next) {
	this->next = next;
}

Information* SymbolTableEntry::getInformation() const{
	return information;
}
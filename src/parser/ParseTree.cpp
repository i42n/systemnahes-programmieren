#include "parser/ParseTree.h"

ParseTree::ParseTree(NodeProg* prog) {
	this->prog = prog;
}

ParseTree::~ParseTree() {
	delete prog;
}

NodeProg* ParseTree::getProg() {
	return prog;
}
#include "parser/Parser.h"
#include "debug.h"

#include <stdlib.h>
#include <typeinfo> // to print class names of nodes
#include <string.h>

Parser::Parser(const char* filename) {
	scanner = new Scanner(filename);
	currentToken = NULL;
	lastToken = NULL;
}

Parser::~Parser() {
	delete scanner;
}

/**
 * parse the file and create an parse tree from it
 * @return a parse tree
 */
ParseTree* Parser::processFile() {
	currentToken = scanner->nextToken();
	NodeProg* prog_node = prog();
	return new ParseTree(prog_node);
}













void Parser::syntax_error(Token::Type expected_type) {
	if(currentToken != NULL) {
		STDERR("Syntax Error. Line: " << currentToken->getLine() << " Column: " << currentToken->getColumn() << "\nExpected: " << Token::token_type_as_string(expected_type) << "\nActual Type: " << Token::token_type_as_string(currentToken->getType()) );
	} else if(lastToken != NULL) {
		STDERR("Syntax Error. Line: " << lastToken->getLine() << " Column: " << (lastToken->getColumn() + strlen(lastToken->getInformation()->getLexem())) << "\nExpected: " << Token::token_type_as_string(expected_type) );
	} else {
		STDERR("Syntax Error! \nExpected: " << Token::token_type_as_string(expected_type) << "\nActual Type: No Token found." );
	}
}

void Parser::syntax_error(const char* expected_type) {
	if(currentToken != NULL) {
		STDERR("Syntax Error. Line: " << currentToken->getLine() << " Column: " << currentToken->getColumn() << "\nExpected: " << expected_type << "\nActual Type: " << Token::token_type_as_string(currentToken->getType()) );
	} else if(lastToken != NULL) {
		STDERR("Syntax Error. Line: " << lastToken->getLine() << " Column: " << (lastToken->getColumn() + strlen(lastToken->getInformation()->getLexem())) << "\nExpected: " << expected_type );
	} else {
		STDERR("Syntax Error! \nExpected: " << expected_type << "\nActual Type: No Token found." );
	}
}














void Parser::match(Token::Type token_type) {
	if(tokenCheck(token_type)) {
		next();
	} else syntax_error(token_type);
}

/**
 * load next token from scanner as current token
 */
void Parser::next() {
	lastToken = currentToken;
	currentToken = scanner->nextToken();
}

/**
 * typecheck current token against given token type
 * @param  type token to compare current token type with
 * @return      true if types match, else false
 */
bool Parser::tokenCheck(Token::Type type) {
	return (currentToken != NULL && currentToken->getType() == type);
}











NodeProg* Parser::prog() {
	NodeProg* prog_node = new NodeProg();
	prog_node->addChild(decls());
	prog_node->addChild(statements());
	return prog_node;
}

NodeDecls* Parser::decls() {
	if(tokenCheck(Token::INT)) {
		NodeDecls* decls_node = new NodeDecls();
		decls_node->addChild(decl());
		match(Token::SIGN_SEMICOLON);
		decls_node->addChild(decls());
		return decls_node;
	} else return new NodeDeclsEpsilon();
	// else epsilon
}

NodeDecl* Parser::decl() {
	NodeDecl* decl_node = new NodeDecl();
	match(Token::INT);
	decl_node->addChild(array());
	if(tokenCheck(Token::IDENTIFIER)) {
		decl_node->addChild(new NodeIdentifier(currentToken));
		next();
	} else syntax_error(Token::IDENTIFIER);
	return decl_node;
}

NodeArray* Parser::array() {
	if(tokenCheck(Token::SQUARE_BRACKET_OPEN)) {
		NodeArray* array_node = new NodeArray();

		match(Token::SQUARE_BRACKET_OPEN);
		if(tokenCheck(Token::INTEGER)) {
			array_node->addChild(new NodeInteger(currentToken));
			next();
		} else syntax_error(Token::INTEGER);
		match(Token::SQUARE_BRACKET_CLOSE);

		return array_node;
	} else return new NodeArrayEpsilon();
	// else epsilon
}

NodeStatements* Parser::statements() {
	if(tokenCheck(Token::IDENTIFIER) ||
		tokenCheck(Token::PRINT) ||
		tokenCheck(Token::READ) ||
		tokenCheck(Token::CURLY_BRACKET_OPEN) ||
		tokenCheck(Token::IF) ||
		tokenCheck(Token::WHILE)) {

		NodeStatements* statements_node = new NodeStatements();

		statements_node->addChild(statement());
		match(Token::SIGN_SEMICOLON);
		statements_node->addChild(statements());

		return statements_node;
	} else return new NodeStatementsEpsilon();
	// else epsilon
}

NodeStatement* Parser::statement() {

	if(tokenCheck(Token::IDENTIFIER)) {
		NodeStatementAssign* statement_assign_node = new NodeStatementAssign();

		if(tokenCheck(Token::IDENTIFIER)) {
			statement_assign_node->addChild(new NodeIdentifier(currentToken));
			next();
		} else syntax_error(Token::IDENTIFIER);

		statement_assign_node->addChild(index());
		match(Token::SIGN_EQUAL);
		statement_assign_node->addChild(exp());

		return statement_assign_node;
	} else if(tokenCheck(Token::PRINT)) {
		NodeStatementPrint* statement_print_node = new NodeStatementPrint();

		match(Token::PRINT);
		match(Token::PARANTHESE_OPEN);
		statement_print_node->addChild(exp());
		match(Token::PARANTHESE_CLOSE);

		return statement_print_node;
	} else if(tokenCheck(Token::READ)) {
		NodeStatementRead* statement_read_node = new NodeStatementRead();

		match(Token::READ);
		match(Token::PARANTHESE_OPEN);

		if(tokenCheck(Token::IDENTIFIER)) {
			statement_read_node->addChild(new NodeIdentifier(currentToken));
			next();
		} else syntax_error(Token::IDENTIFIER);

		statement_read_node->addChild(index());
		match(Token::PARANTHESE_CLOSE);

		return statement_read_node;
	} else if(tokenCheck(Token::CURLY_BRACKET_OPEN)) {
		NodeStatementBlock* statement_block_node = new NodeStatementBlock();

		match(Token::CURLY_BRACKET_OPEN);
		statement_block_node->addChild(statements());
		match(Token::CURLY_BRACKET_CLOSE);

		return statement_block_node;
	} else if (tokenCheck(Token::IF)) {
		NodeStatementIfElse* statement_if_else_node = new NodeStatementIfElse();

		match(Token::IF);
		match(Token::PARANTHESE_OPEN);
		statement_if_else_node->addChild(exp());
		match(Token::PARANTHESE_CLOSE);
		statement_if_else_node->addChild(statement());
		match(Token::ELSE);
		statement_if_else_node->addChild(statement());

		return statement_if_else_node;
	} else if (tokenCheck(Token::WHILE)) {
		NodeStatementWhile* statement_while_node = new NodeStatementWhile();

		match(Token::WHILE);
		match(Token::PARANTHESE_OPEN);
		statement_while_node->addChild(exp());
		match(Token::PARANTHESE_CLOSE);
		statement_while_node->addChild(statement());

		return statement_while_node;
	} else {
		syntax_error("STATEMENT");
		return NULL;
	}
}

NodeExp* Parser::exp() {
	NodeExp* exp_node = new NodeExp();

	exp_node->addChild(exp2());
	exp_node->addChild(op_exp());

	return exp_node;
}

NodeExp2* Parser::exp2() {

	if(tokenCheck(Token::PARANTHESE_OPEN)) {
		NodeExp2Exp* exp2_exp_node = new NodeExp2Exp();

		match(Token::PARANTHESE_OPEN);
		exp2_exp_node->addChild(exp());
		match(Token::PARANTHESE_CLOSE);

		return exp2_exp_node;
	} else if(tokenCheck(Token::IDENTIFIER)) {
		NodeExp2IdentifierIndex* exp2_identifier_index_node = new NodeExp2IdentifierIndex();

		if(tokenCheck(Token::IDENTIFIER)) {
			exp2_identifier_index_node->addChild(new NodeIdentifier(currentToken));
			next();
		} else syntax_error(Token::IDENTIFIER);

		exp2_identifier_index_node->addChild(index());

		return exp2_identifier_index_node;
	} else if(tokenCheck(Token::INTEGER)) {
		NodeExp2Integer* exp2_integer_node = new NodeExp2Integer();

		if(tokenCheck(Token::INTEGER)) {
			exp2_integer_node->addChild(new NodeInteger(currentToken));
			next();
		} else syntax_error(Token::INTEGER);

		return exp2_integer_node;
	} else if(tokenCheck(Token::SIGN_MINUS)) {
		NodeExp2NegativeExp* exp2_negative_exp_node = new NodeExp2NegativeExp();

		match(Token::SIGN_MINUS);
		exp2_negative_exp_node->addChild(exp2());

		return exp2_negative_exp_node;
	} else if(tokenCheck(Token::SIGN_NOT)) {
		NodeExp2NotExp* exp2_not_exp_node = new NodeExp2NotExp();

		match(Token::SIGN_NOT);
		exp2_not_exp_node->addChild(exp2());

		return exp2_not_exp_node;
	} else {
		syntax_error("EXP2");
		return NULL;
	}
}

NodeIndex* Parser::index() {
	if(tokenCheck(Token::SQUARE_BRACKET_OPEN)) {
		NodeIndex* index_node = new NodeIndex();
		
		match(Token::SQUARE_BRACKET_OPEN);
		index_node->addChild(exp());
		match(Token::SQUARE_BRACKET_CLOSE);

		return index_node;
	} else return new NodeIndexEpsilon();
	// else epsilon
}

NodeOpExp* Parser::op_exp() {
	if(tokenCheck(Token::SIGN_PLUS) ||
		tokenCheck(Token::SIGN_MINUS) ||
		tokenCheck(Token::SIGN_STAR) ||
		tokenCheck(Token::SIGN_SLASH) ||
		tokenCheck(Token::SIGN_SMALLER) ||
		tokenCheck(Token::SIGN_LARGER) ||
		tokenCheck(Token::SIGN_DOUBLE_EQUAL) ||
		tokenCheck(Token::SIGN_UNEQUAL) ||
		tokenCheck(Token::SIGN_AND)) {

		NodeOpExp* op_exp_node = new NodeOpExp();

		op_exp_node->addChild(op());
		op_exp_node->addChild(exp());

		return op_exp_node;
	} else return new NodeOpExpEpsilon();
	// else epsilon
}

NodeOp* Parser::op() {
	if(tokenCheck(Token::SIGN_PLUS) ||
		tokenCheck(Token::SIGN_MINUS) ||
		tokenCheck(Token::SIGN_STAR) ||
		tokenCheck(Token::SIGN_SLASH) ||
		tokenCheck(Token::SIGN_SMALLER) ||
		tokenCheck(Token::SIGN_LARGER) ||
		tokenCheck(Token::SIGN_DOUBLE_EQUAL) ||
		tokenCheck(Token::SIGN_UNEQUAL) ||
		tokenCheck(Token::SIGN_AND)) {

		NodeOp* op = new NodeOp(currentToken);
		next();
		return op;

	} else {
		syntax_error("OP");
		return NULL;
	}

}
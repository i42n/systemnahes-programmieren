#include "parser/node/Node.h"
#include "debug.h"

Node::Node() {
	nodeType = TYPE_NONE;
}
Node::~Node() {}

void Node::addChild(Node* node) {
	STDERR("Unable to resolve specific Node-Sub-Type... quitting");
}

void Node::setType(NodeType newType) {
	nodeType = newType;
}

NodeType Node::getType() {
	return nodeType;
}

NodeLeaf::NodeLeaf(){}
NodeLeaf::~NodeLeaf(){} 

NodeStatement::NodeStatement(){}
NodeStatement::~NodeStatement(){}

NodeExp2::NodeExp2(){}
NodeExp2::~NodeExp2(){}

void Node::accept(Visitor* visitor) {
	visitor->visit(this);
}

void NodeLeaf::accept(Visitor* visitor) {
	visitor->visit(this);
}

void NodeExp2::accept(Visitor* visitor) {
	visitor->visit(this);
}

void NodeStatement::accept(Visitor* visitor) {
	visitor->visit(this);
}
#include "parser/CodeGenerator.h"
#include "debug.h"

CodeGenerator::CodeGenerator() {
	//output_file = NULL;
	counter = 0;
}

CodeGenerator::~CodeGenerator() {
	code.close();
}

/**
 * print tree in json format
 * for nice formatting use http://jsonprettyprint.com
 * @param output_filename writes the json to this file
 */
void CodeGenerator::makeCode(char* output_filename, ParseTree* tree) {
	code.open (output_filename); // TODO add error handling here

	tree->getProg()->accept(this);

	code.close();
}

int CodeGenerator::getLabelCounter () {
	return counter++;
}




















void CodeGenerator::visit(Node* node){
	STDERR("ERROR: bad node resolution on Code generation.");
}
void CodeGenerator::visit(NodeStatement* node){
	STDERR("ERROR: bad node resolution on Code generation.");
}
void CodeGenerator::visit(NodeLeaf* node){
	STDERR("ERROR: bad node resolution on Code generation.");
}
void CodeGenerator::visit(NodeExp2* node){
	STDERR("ERROR: bad node resolution on Code generation.");
}
















// PROG ::= DELCS STATEMENTS
void CodeGenerator::visit(NodeProg* node) {
	node->getDeclarations()->accept(this);
	node->getStatements()->accept(this);
	code << "STP" << std::endl;
}

// DECLS ::= DECL;DECLS
void CodeGenerator::visit(NodeDecls* node) {
	node->getDeclaration()->accept(this);
	node->getNextDeclarations()->accept(this);
}

void CodeGenerator::visit(NodeDeclsEpsilon* node){}

// DECL ::= int ARRAY identifier
void CodeGenerator::visit(NodeDecl* node){
	code << "DS "<< "$" <<node->getIdentifier()->getLexem() << " ";
	node->getArray()->accept(this);
	code << std::endl;
}

// ARRAY ::= [integer]
void CodeGenerator::visit(NodeArray* node){
	code << node->getInteger()->getValue();
}

void CodeGenerator::visit(NodeArrayEpsilon* node){
	code << 1;
}

// STATEMENTS ::= STATEMENT;STATEMENTS
void CodeGenerator::visit(NodeStatements* node){
	node->getStatement()->accept(this);
	node->getStatements()->accept(this);
}

void CodeGenerator::visit(NodeStatementsEpsilon* node){
	code << "NOP" << std::endl;
}

// STATEMENT ::= identifier INDEX = EXP
void CodeGenerator::visit(NodeStatementAssign* node){
	node->getExpression()->accept(this);
	code << "LA " << "$" << node->getIdentifier()->getLexem() << std::endl;
	node->getIndex()->accept(this);
	code << "STR" << std::endl;
}

// STATEMENT ::= print(EXP)
void CodeGenerator::visit(NodeStatementPrint* node){
	node->getExpression()->accept(this);
	code << "PRI" << std::endl;
}

// STATEMENT ::= read(identifier INDEX)
void CodeGenerator::visit(NodeStatementRead* node){
	code << "REA" << std::endl;
	code << "LA " << "$" << node->getIdentifier()->getLexem() << std::endl;
	node->getIndex()->accept(this);
	code << "STR" << std::endl;
}

// STATEMENT ::= {STATEMENTS}
void CodeGenerator::visit(NodeStatementBlock* node){
	node->getStatements()->accept(this);
}

// STATEMENT ::= if (EXP) STATEMENT else STATEMENT
void CodeGenerator::visit(NodeStatementIfElse* node){
	node->getExpression()->accept(this);
	int labelIf = getLabelCounter();
	code << "JIN " << "#label" << labelIf << std::endl; 

	node->getIfStatement()->accept(this);
	int labelElse = getLabelCounter();
	code << "JMP " << "#label" << labelElse << std::endl;
	code << "#label"<< labelIf << " NOP" << std::endl;

	node->getElseStatement()->accept(this);
	code << "#label" << labelElse << " NOP" << std::endl;
}

// STATEMENT ::= while (EXP) STATEMENT
void CodeGenerator::visit(NodeStatementWhile* node){
	int labelWhile1 = getLabelCounter();
	code << "#label" << labelWhile1 << " NOP" << std::endl;

	node->getExpression()->accept(this);
	int labelWhile2 = getLabelCounter();
	code << "JIN " << "#label" << labelWhile2 << std::endl;

	node->getStatement()->accept(this);
	code << "JMP " << "#label" << labelWhile1 << std::endl;
	code << "#label" << labelWhile2 << " NOP" << std::endl;
}

// EXP ::= EXP2 OP_EXP
void CodeGenerator::visit(NodeExp* node){
	if (node->getType() == TYPE_NONE){
		node->getExpression()->accept(this);
	}
	// check for NULL because OP_EXP can be OP_EXP_EPSILON, and then getOperation returns NULL
	else if (node->getOperatorExpression()->getOperation() != NULL && node->getOperatorExpression()->getOperation()->getType() == TYPE_OP_GREATER) {
		node->getOperatorExpression()->accept(this);
		node->getExpression()->accept(this);
		code << "LES" << std::endl;
	}
	// check for NULL because OP_EXP can be OP_EXP_EPSILON, and then getOperation returns NULL
	else if (node->getOperatorExpression()->getOperation() != NULL && node->getOperatorExpression()->getOperation()->getType() == TYPE_OP_UNEQUAL) {
		node->getExpression()->accept(this);
		node->getOperatorExpression()->accept(this);
		code << "NOT" << std::endl;		
	}
	else {
		node->getExpression()->accept(this);
		node->getOperatorExpression()->accept(this);
	}
}

// INDEX ::= [EXP]
void CodeGenerator::visit(NodeIndex* node){
	node->getExpression()->accept(this);
	code << "ADD" << std::endl;
}

void CodeGenerator::visit(NodeIndexEpsilon* node){}

// EXP2 ::= EXP
void CodeGenerator::visit(NodeExp2Exp* node){
	node->getExpression()->accept(this);	
}

// EXP2 ::= identifier INDEX
void CodeGenerator::visit(NodeExp2IdentifierIndex* node){
	code << "LA " << "$" << node->getIdentifier()->getLexem() << std::endl;
	node->getIndex()->accept(this);
	code << "LV" << std::endl;
}

// EXP2 ::= integer
void CodeGenerator::visit(NodeExp2Integer* node){
	code << "LC " << node->getInteger()->getValue() << std::endl;
}

// EXP2 ::= - EXP
void CodeGenerator::visit(NodeExp2NegativeExp* node){
	code << "LC " << 0 << std::endl;
	node->getExpression()->accept(this);
	code << "SUB" << std::endl;
}

// EXP2 ::= ! EXP
void CodeGenerator::visit(NodeExp2NotExp* node){
	node->getExpression()->accept(this);
	code << "NOT" << std::endl;
}

// OP_EXP ::= OP EXP
void CodeGenerator::visit(NodeOpExp* node){
	node->getExpression()->accept(this);
	node->getOperation()->accept(this);
}

void CodeGenerator::visit(NodeOpExpEpsilon* node){}





/***********************
 *
 * LEAFS
 * 
 ***********************/

// OP
void CodeGenerator::visit(NodeOp* node){
	switch (node->getType()) {
		case TYPE_OP_PLUS: {
			code << "ADD" << std::endl;
			break;
		}
		case TYPE_OP_MINUS: {
			code << "SUB" << std::endl;
			break;
		}
		case TYPE_OP_MULTIPLY: {
			code << "MUL" << std::endl;
			break;
		}
		case TYPE_OP_DIVIDE: {
			code << "DIV" << std::endl;
			break;
		}
		case TYPE_OP_SMALLER: {
			code << "LES" << std::endl;
			break;
		}
		case TYPE_OP_GREATER: {
			break;
		}
		case TYPE_OP_COMPARE: {
			code << "EQU" << std::endl;
			break;
		}
		case TYPE_OP_UNEQUAL: {
			code << "EQU" << std::endl;
			break;
		}
		case TYPE_OP_AND: {
			code << "AND" << std::endl;
			break;
		}
		default:
			STDERR("Node type unknown.");
	}
}

void CodeGenerator::visit(NodeIdentifier* node){}

void CodeGenerator::visit(NodeInteger* node){}


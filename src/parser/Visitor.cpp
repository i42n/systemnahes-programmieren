#include "parser/Visitor.h"

void visit(Node* node){}
void visit(NodeStatement* node){}
void visit(NodeLeaf* node){}
void visit(NodeExp2* node){}

void Visitor::visit(NodeProg* node){}
void Visitor::visit(NodeDecls* node){}
void Visitor::visit(NodeDecl* node){}
void Visitor::visit(NodeArray* node){}
void Visitor::visit(NodeStatements* node){}
void Visitor::visit(NodeStatementAssign* node){}
void Visitor::visit(NodeStatementPrint* node){}
void Visitor::visit(NodeStatementRead* node){}
void Visitor::visit(NodeStatementBlock* node){}
void Visitor::visit(NodeStatementIfElse* node){}
void Visitor::visit(NodeStatementWhile* node){}

void Visitor::visit(NodeExp* node){}
void Visitor::visit(NodeExp2Exp* node){}
void Visitor::visit(NodeExp2IdentifierIndex* node){}
void Visitor::visit(NodeExp2Integer* node){}
void Visitor::visit(NodeExp2NegativeExp* node){}
void Visitor::visit(NodeExp2NotExp* node){}

void Visitor::visit(NodeIndex* node){}
void Visitor::visit(NodeOpExp* node){}
void Visitor::visit(NodeOp* node){}

void Visitor::visit(NodeIdentifier* node){}
void Visitor::visit(NodeInteger* node){}

void Visitor::visit(NodeDeclsEpsilon* node){}
void Visitor::visit(NodeArrayEpsilon* node){}
void Visitor::visit(NodeStatementsEpsilon* node){}
void Visitor::visit(NodeOpExpEpsilon* node){}
void Visitor::visit(NodeIndexEpsilon* node){}
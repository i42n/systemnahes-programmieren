#include "parser/JsonGenerator.h"
#include "debug.h"

JsonGenerator::JsonGenerator() {
	//output_file = NULL;
}

JsonGenerator::~JsonGenerator() {
	output_file.close();
}

/**
 * print tree in json format
 * for nice formatting use http://jsonprettyprint.com
 * @param output_filename writes the json to this file
 */
void JsonGenerator::makeJSON(char* output_filename, ParseTree* tree) {
	output_file.open (output_filename); // TODO add error handling here

	tree->getProg()->accept(this);

	output_file.close();
}












void JsonGenerator::visit(Node* node){
	STDERR("ERROR: bad node resolution on JSON generation.");
}
void JsonGenerator::visit(NodeStatement* node){
	STDERR("ERROR: bad node resolution on JSON generation.");
}
void JsonGenerator::visit(NodeLeaf* node){
	STDERR("ERROR: bad node resolution on JSON generation.");
}
void JsonGenerator::visit(NodeExp2* node){
	STDERR("ERROR: bad node resolution on JSON generation.");
}



void JsonGenerator::visit(NodeDeclsEpsilon* node){
		output_file << "{";

		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		
		output_file << "}";
}

void JsonGenerator::visit(NodeArrayEpsilon* node){
		output_file << "{";

		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		
		output_file << "}";
}

void JsonGenerator::visit(NodeStatementsEpsilon* node){
		output_file << "{";

		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		
		output_file << "}";
}

void JsonGenerator::visit(NodeOpExpEpsilon* node){
		output_file << "{";

		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		
		output_file << "}";
}

void JsonGenerator::visit(NodeIndexEpsilon* node){
		output_file << "{";

		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		
		output_file << "}";
}




// PROG ::= DELCS STATEMENTS
void JsonGenerator::visit(NodeProg* node) {
		output_file << "{";

		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";
		
		// print children
		output_file << ", \"children\": [ ";
		node->getDeclarations()->accept(this);
		output_file << ", ";
		node->getStatements()->accept(this);
		output_file << "]";
		
		output_file << "}";
}

// DECLS ::= DECL;DECLS
void JsonGenerator::visit(NodeDecls* node) {
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";
		
		// print children
		output_file << ", \"children\": [ " ;
		node->getDeclaration()->accept(this);
		output_file << ", ";
		node->getNextDeclarations()->accept(this);

		output_file << "]";

		output_file << "}";
}

// DECL ::= int ARRAY identifier
void JsonGenerator::visit(NodeDecl* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;
		node->getArray()->accept(this);
		output_file << ", ";
		node->getIdentifier()->accept(this);
		
		output_file << "]";

		output_file << "}";
}

// ARRAY ::= [integer]
void JsonGenerator::visit(NodeArray* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;
		node->getInteger()->accept(this);
		output_file << "]";

		output_file << "}";
}

// STATEMENTS ::= STATEMENT;STATEMENTS
void JsonGenerator::visit(NodeStatements* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;

		node->getStatement()->accept(this);
		output_file << ", ";
		node->getStatements()->accept(this);
		
		output_file << "]";

		output_file << "}";
}

// STATEMENT ::= identifier INDEX = EXP
void JsonGenerator::visit(NodeStatementAssign* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;

		node->getIdentifier()->accept(this);
		output_file << ", ";
		node->getIndex()->accept(this);
		output_file << ", ";
		node->getExpression()->accept(this);
		
		output_file << "]";

		output_file << "}";
}

// STATEMENT ::= print(EXP)
void JsonGenerator::visit(NodeStatementPrint* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;

		node->getExpression()->accept(this);
		output_file << "]";

		output_file << "}";
}

// STATEMENT ::= read(identifier INDEX)
void JsonGenerator::visit(NodeStatementRead* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;
		node->getIdentifier()->accept(this);
		output_file << ", ";
		node->getIndex()->accept(this);

		output_file << "]";

		output_file << "}";
}

// STATEMENT ::= {STATEMENTS}
void JsonGenerator::visit(NodeStatementBlock* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;
		node->getStatements()->accept(this);
		output_file << "]";

		output_file << "}";
}

// STATEMENT ::= if (EXP) STATEMENT else STATEMENT
void JsonGenerator::visit(NodeStatementIfElse* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;

		node->getExpression()->accept(this);
		output_file << ", ";
		node->getIfStatement()->accept(this);
		output_file << ", ";
		node->getElseStatement()->accept(this);
		output_file << "]";


		output_file << "}";
}

// STATEMENT ::= while (EXP) STATEMENT
void JsonGenerator::visit(NodeStatementWhile* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;

		node->getExpression()->accept(this);
		output_file << ", ";
		node->getStatement()->accept(this);
		
		output_file << "]";

		output_file << "}";
}

// EXP ::= EXP2 OP_EXP
void JsonGenerator::visit(NodeExp* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;

		node->getExpression()->accept(this);
		output_file << ", ";
		node->getOperatorExpression()->accept(this);
		
		output_file << "]";

		output_file << "}";
}

// EXP2 ::= EXP
void JsonGenerator::visit(NodeExp2Exp* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;

		node->getExpression()->accept(this);
		output_file << "]";

		output_file << "}";
}

// EXP2 ::= identifier INDEX
void JsonGenerator::visit(NodeExp2IdentifierIndex* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;

		node->getIdentifier()->accept(this);
		output_file << ", ";
		node->getIndex()->accept(this);
		
		output_file << "]";

		output_file << "}";
}

// EXP2 ::= integer
void JsonGenerator::visit(NodeExp2Integer* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;
		node->getInteger()->accept(this);
		output_file << "]";

		output_file << "}";
}

// EXP2 ::= - EXP
void JsonGenerator::visit(NodeExp2NegativeExp* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;
		node->getExpression()->accept(this);
		output_file << "]";

		output_file << "}";
}

// EXP2 ::= ! EXP
void JsonGenerator::visit(NodeExp2NotExp* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;
		node->getExpression()->accept(this);
		output_file << "]";

		output_file << "}";
}

// INDEX ::= [EXP]
void JsonGenerator::visit(NodeIndex* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;
		node->getExpression()->accept(this);
		output_file << "]";

		output_file << "}";
}

// OP_EXP ::= OP EXP
void JsonGenerator::visit(NodeOpExp* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		
		// print children
		output_file << ", \"children\": [ " ;
		node->getOperation()->accept(this);
		output_file << ", ";
		node->getExpression()->accept(this);
		
		output_file << "]";

		output_file << "}";
}





/***********************
 *
 * LEAFS
 * 
 ***********************/

// OP
void JsonGenerator::visit(NodeOp* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		output_file << ", ";
		output_file << "\"_op_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		output_file << "}";
}

// identifier
void JsonGenerator::visit(NodeIdentifier* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		output_file << ", ";
		output_file << "\"_lexem\":" << "\"" << node->getLexem() << "\"";

		output_file << "}";
}

// integer
void JsonGenerator::visit(NodeInteger* node){
		output_file << "{";
		
		// print this nodes data
		output_file << "\"_node_class\":" << "\"" << typeid(node).name() << "\"";
		output_file << ", ";
		output_file << "\"_node_type\":" << "\"" << Node::node_type_as_string(node->getType()) << "\"";

		output_file << ", ";
		output_file << "\"_value\":" << "\"" << node->getValue() << "\"";

		output_file << "}";
}
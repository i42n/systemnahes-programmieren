#include "parser/Parser.h"
#include "parser/ParseTree.h"
#include "parser/SemanticAnalyzer.h"
#include "parser/CodeGenerator.h"
#include "parser/JsonGenerator.h"
#include "parser/node/Node.h"

#include <iostream>

/**
 * param1: input file
 * param2: tree as json output
 * param3: code output
 */
int main(int argc, char **argv) {

	if(argc == 5) {
		std::cout << "parsing..." << std::endl;
		Parser parser = Parser(argv[1]);
		ParseTree* tree = parser.processFile();

		// print generated tree as json
		std::cout << "json tree exporting..." << std::endl;
		JsonGenerator json_generator;
		json_generator.makeJSON(argv[2], tree);
		
		// type checking
		std::cout << "type checking..." << std::endl;
		SemanticAnalyzer type_checker;
		type_checker.check(tree);
		std::cout << "json tree exporting..." << std::endl;
		json_generator.makeJSON(argv[3], tree);

		// generate code
		std::cout << "generating code..." << std::endl;
		CodeGenerator code_generator;
		code_generator.makeCode(argv[4], tree);
		
	} else {
		std::cout << "Please call the test with the correct syntax." << std::endl;
	}

}


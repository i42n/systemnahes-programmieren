#include "parser/Parser.h"
#include "parser/ParseTree.h"
#include "parser/SemanticAnalyzer.h"
#include "parser/CodeGenerator.h"
#include "parser/JsonGenerator.h"
#include "parser/node/Node.h"

#include <iostream>

/**
 * param1: input file
 * param2: code output
 */
int main(int argc, char **argv) {

	if(argc == 3) {
		std::cout << "parsing..." << std::endl;
		Parser parser = Parser(argv[1]);
		ParseTree* tree = parser.processFile();
		
		std::cout << "type checking..." << std::endl;
		SemanticAnalyzer type_checker;
		type_checker.check(tree);

		// generate code
		std::cout << "generating code..." << std::endl;
		CodeGenerator code_generator;
		code_generator.makeCode(argv[2], tree);
		
	} else {
		std::cout << "Please call the compiler with the correct syntax." << std::endl;
	}

}


#include "parser/SemanticAnalyzer.h"
#include "debug.h"

SemanticAnalyzer::SemanticAnalyzer() {

}

SemanticAnalyzer::~SemanticAnalyzer() {

}

void SemanticAnalyzer::check(ParseTree* tree) {
	tree->getProg()->accept(this);
}






void SemanticAnalyzer::visit(Node* node){
	STDERR("ERROR: bad node resolution on JSON generation.");
}
void SemanticAnalyzer::visit(NodeStatement* node){
	STDERR("ERROR: bad node resolution on JSON generation.");
}
void SemanticAnalyzer::visit(NodeLeaf* node){
	STDERR("ERROR: bad node resolution on JSON generation.");
}
void SemanticAnalyzer::visit(NodeExp2* node){
	STDERR("ERROR: bad node resolution on JSON generation.");
}
void SemanticAnalyzer::visit(NodeIdentifier* node){
	STDERR("ERROR: bad node resolution on JSON generation.");
}
void SemanticAnalyzer::visit(NodeInteger* node){
	STDERR("ERROR: bad node resolution on JSON generation.");
}








// PROG ::= DECLS STATEMENTS
void SemanticAnalyzer::visit(NodeProg* node) {
	node -> getDeclarations() -> accept(this);
	node -> getStatements() -> accept(this);
	
	//node -> setType(TYPE_NONE);
}

// DECLS ::= DECL;DECLS
void SemanticAnalyzer::visit(NodeDecls* node) {
	node -> getDeclaration() -> accept(this);
	node -> getNextDeclarations() -> accept(this);
	
	//node -> setType(TYPE_NONE);
}

void SemanticAnalyzer::visit(NodeDeclsEpsilon* node){
	//node -> setType(TYPE_NONE);
}

// DECL ::= int ARRAY identifier
void SemanticAnalyzer::visit(NodeDecl* node){
	
	node -> getArray() -> accept(this);
		
	if(node->getIdentifier()->getType() != TYPE_NONE){
		// error
		STDMESSAGE("Fehler in Zeile: " << node->getIdentifier()->getLine());
		STDMESSAGE("Fehler in Spalte: " << node->getIdentifier()->getColumn());
		STDERR("Fehler: Identifier already defined");
		node -> setType(TYPE_ERROR);
	} else if (node->getArray()->getType() == TYPE_ERROR){
		node -> setType(TYPE_ERROR);
	} else {
		//node -> setType(TYPE_NONE);
	
		if(node->getArray()->getType() == TYPE_ARRAY){
			// Das ist eigentlich das store
			node->getIdentifier()->setType(TYPE_INTEGER_ARRAY); // Typ-information speichern
		} else{
			// Das ist eigentlich das store
			node->getIdentifier()->setType(TYPE_INTEGER); // Typ-Information speichern
		}
	}

}

// ARRAY ::= [integer]
void SemanticAnalyzer::visit(NodeArray* node){
	if(node->getInteger()->getValue() > 0){
		node -> setType(TYPE_ARRAY);
	} else{
		// error
		STDMESSAGE("Fehler in Zeile: " << node->getInteger()->getLine());
		STDMESSAGE("Fehler in Spalte: " << node->getInteger()->getColumn());
		STDERR("Fehler: No valid dimension");
		node -> setType(TYPE_ERROR);
	}
}

void SemanticAnalyzer::visit(NodeArrayEpsilon* node){
	//node -> setType(TYPE_NONE);
}

// STATEMENTS ::= STATEMENT;STATEMENTS
void SemanticAnalyzer::visit(NodeStatements* node){
	node -> getStatement() -> accept(this);
	node -> getStatements() -> accept(this);
	
	//node -> setType(TYPE_NONE);
}

void SemanticAnalyzer::visit(NodeStatementsEpsilon* node){
	//node -> setType(TYPE_NONE);
}

// STATEMENT ::= identifier INDEX = EXP
void SemanticAnalyzer::visit(NodeStatementAssign* node){
	node -> getExpression() -> accept(this);
	node -> getIndex() -> accept(this);
	
	if(node->getIdentifier()->getType() == TYPE_NONE){
		// error
		STDMESSAGE("Fehler in Zeile: " << node->getIdentifier()->getLine());
		STDMESSAGE("Fehler in Spalte: " << node->getIdentifier()->getColumn());
		STDERR("Fehler: Identifier not defined");
		node -> setType(TYPE_ERROR);
	} else if(node->getExpression()->getType() == TYPE_INTEGER && (
			( node->getIdentifier()->getType() == TYPE_INTEGER && node->getIndex()->getType() == TYPE_NONE) || 
			( node->getIdentifier()->getType() == TYPE_INTEGER_ARRAY && node->getIndex()->getType() == TYPE_ARRAY)) ){
		//node -> setType(TYPE_NONE);
	}
	else{
		// error
		STDMESSAGE("Fehler in Zeile: " << node->getIdentifier()->getLine());
		STDMESSAGE("Fehler in Spalte: " << node->getIdentifier()->getColumn());
		STDERR("Fehler: Incompatible types");
		
		node -> setType(TYPE_ERROR);
	}

}

// STATEMENT ::= print(EXP)
void SemanticAnalyzer::visit(NodeStatementPrint* node){
	node -> getExpression() -> accept(this);
	//node -> setType(TYPE_NONE);
}

// STATEMENT ::= read(identifier INDEX)
void SemanticAnalyzer::visit(NodeStatementRead* node){
	node -> getIndex() -> accept(this);
	
	if(node->getIdentifier()->getType() == TYPE_NONE){
		// error
		STDMESSAGE("Fehler in Zeile: " << node->getIdentifier()->getLine());
		STDMESSAGE("Fehler in Spalte: " << node->getIdentifier()->getColumn());
		STDERR("Fehler: Identifier not defined");
		node -> setType(TYPE_ERROR);
	} else if( ( (node->getIdentifier()->getType() == TYPE_INTEGER ) && node->getIndex()->getType() == TYPE_NONE) ||
			( (node->getIdentifier()->getType() == TYPE_INTEGER_ARRAY) && node->getIndex()->getType() == TYPE_ARRAY)){
		//node -> setType(TYPE_NONE);
	} else{
		// error
		STDMESSAGE("Fehler in Zeile: " << node->getIdentifier()->getLine());
		STDMESSAGE("Fehler in Spalte: " << node->getIdentifier()->getColumn());
		STDERR("Fehler: Incompatible types");
		node -> setType(TYPE_ERROR);
	}
}

// STATEMENT ::= {STATEMENTS}
void SemanticAnalyzer::visit(NodeStatementBlock* node){
	node -> getStatements() -> accept(this);
	//node -> setType(TYPE_NONE);
}

// STATEMENT ::= if (EXP) STATEMENT else STATEMENT
void SemanticAnalyzer::visit(NodeStatementIfElse* node){
	node -> getExpression() -> accept(this);
	node -> getIfStatement() -> accept(this);
	node -> getElseStatement() -> accept(this);
	
	if(node->getExpression()->getType() == TYPE_ERROR){
		node -> setType(TYPE_ERROR);
	} 
	// else{
	// 	node -> setType(TYPE_NONE);
	// }
}

// STATEMENT ::= while (EXP) STATEMENT
void SemanticAnalyzer::visit(NodeStatementWhile* node){
	node -> getExpression() -> accept(this);
	node -> getStatement() -> accept(this);
	
	if(node->getExpression()->getType() == TYPE_ERROR){
		node -> setType(TYPE_ERROR);
	}
	// else{
	// 	node -> setType(TYPE_NONE);
	// }
}

// INDEX ::= [EXP]
void SemanticAnalyzer::visit(NodeIndex* node){
	node -> getExpression() -> accept(this);
	
	if(node->getExpression()->getType() == TYPE_ERROR){
		node -> setType(TYPE_ERROR);
	} else{
		node -> setType(TYPE_ARRAY);
	}
}

void SemanticAnalyzer::visit(NodeIndexEpsilon* node){
	//node -> setType(TYPE_NONE);
}

// EXP ::= EXP2 OP_EXP
void SemanticAnalyzer::visit(NodeExp* node){
	node -> getExpression() -> accept(this);
	node -> getOperatorExpression() -> accept(this);
	
	if(node->getOperatorExpression()->getType() == TYPE_NONE){
		node -> setType(node->getExpression()->getType());
	} else if(node->getExpression()->getType() != node->getOperatorExpression()->getType()){
		node -> setType(TYPE_ERROR);
	} else {
		node -> setType(node->getExpression()->getType());
	}	
}

// EXP2 ::= (EXP)
void SemanticAnalyzer::visit(NodeExp2Exp* node){
	node -> getExpression() -> accept(this);
	node -> setType(node->getExpression()->getType());
}

// EXP2 ::= identifier INDEX
void SemanticAnalyzer::visit(NodeExp2IdentifierIndex* node){
	node -> getIndex() -> accept(this);
	
	if(node->getIdentifier()->getType() == TYPE_NONE){
		// error
		STDMESSAGE("Fehler in Zeile: " << node->getIdentifier()->getLine());
		STDMESSAGE("Fehler in Spalte: " << node->getIdentifier()->getColumn());
		STDERR("Fehler: Identifier not defined");
		node -> setType(TYPE_ERROR);
	} else if(node->getIdentifier()->getType() == TYPE_INTEGER && node->getIndex()->getType() == TYPE_NONE){
		node -> setType(node->getIdentifier()->getType());
	} else if(node->getIdentifier()->getType() == TYPE_INTEGER_ARRAY && node->getIndex()->getType() == TYPE_ARRAY){
		node -> setType(TYPE_INTEGER);
	} else{
		// error
		STDMESSAGE("Fehler in Zeile: " << node->getIdentifier()->getLine());
		STDMESSAGE("Fehler in Spalte: " << node->getIdentifier()->getColumn());
		STDERR("Fehler: No primitive Type");
		node -> setType(TYPE_ERROR);
	}
}

// EXP2 ::= integer
void SemanticAnalyzer::visit(NodeExp2Integer* node){
	node -> setType(TYPE_INTEGER);
}

// EXP2 ::= -EXP2
void SemanticAnalyzer::visit(NodeExp2NegativeExp* node){
	node -> getExpression() -> accept(this);
	node -> setType(node->getExpression()->getType());
}

// EXP2 ::= !EXP2
void SemanticAnalyzer::visit(NodeExp2NotExp* node){
	node -> getExpression() -> accept(this);
	
	if(node->getExpression()->getType() != TYPE_INTEGER){
		node -> setType(TYPE_ERROR);
	} else{
		node -> setType(TYPE_INTEGER);
	}
}

// OP_EXP ::= OP EXP
void SemanticAnalyzer::visit(NodeOpExp* node){
	node -> getOperation() -> accept(this);
	node -> getExpression() -> accept(this);
	
	node -> setType(node->getExpression()->getType());
}

void SemanticAnalyzer::visit(NodeOpExpEpsilon* node){
	//node -> setType(TYPE_NONE);
}

// OP ::= '+', '-', '/', '*', '>', '<', '=!=', '==', '&'
void SemanticAnalyzer::visit(NodeOp* node){
	// nothing to do. OP types already set by parser when OP object was created.
}





















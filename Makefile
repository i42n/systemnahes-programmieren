###########################
# Global Settings
###########################

SOURCE_DIR = src
DOCU_DIR = doc

BUFFERDIR = $(SOURCE_DIR)/buffer
SYMBOLTABLEDIR = $(SOURCE_DIR)/symboltable
AUTOMATDIR = $(SOURCE_DIR)/automat
SCANNERDIR = $(SOURCE_DIR)/scanner
PARSERDIR = $(SOURCE_DIR)/parser

LOADLIBES = -pthread
export LOADLIBES

###########################
# Targets
###########################

.PHONY: doc all clean buffer buffer_test scanner scanner_test automat automat_test symboltable symboltable_test parser parser_test

all: buffer_test automat_test scanner_test symboltable_test stringtable_test parser_test compiler

buffer_test:
	$(MAKE) -C $(BUFFERDIR) buffer_test

buffer:
	$(MAKE) -C $(BUFFERDIR) buffer

automat_test:
	$(MAKE) -C $(AUTOMATDIR) automat_test

automat:
	$(MAKE) -C $(AUTOMATDIR) automat

stringtable_test:
	$(MAKE) -C $(SYMBOLTABLEDIR) stringtable_test

symboltable_test: scanner
	$(MAKE) -C $(SYMBOLTABLEDIR) symboltable_test

symboltable:
	$(MAKE) -C $(SYMBOLTABLEDIR) symboltable

scanner_test: buffer automat symboltable
	$(MAKE) -C $(SCANNERDIR) scanner_test

scanner:
	$(MAKE) -C $(SCANNERDIR) scanner

parser_test: buffer automat symboltable scanner
	$(MAKE) -C $(PARSERDIR) parser_test

compiler: buffer automat symboltable scanner
	$(MAKE) -C $(PARSERDIR) compiler

parser:
	$(MAKE) -C $(PARSERDIR) parser

doc:
	$(MAKE) -C $(DOCU_DIR)

clean:
	$(MAKE) -C $(BUFFERDIR) clean
	$(MAKE) -C $(AUTOMATDIR) clean
	$(MAKE) -C $(SCANNERDIR) clean
	$(MAKE) -C $(SYMBOLTABLEDIR) clean
	$(MAKE) -C $(PARSERDIR) clean
	$(MAKE) -C $(DOCU_DIR) clean


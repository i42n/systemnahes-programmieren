#!/bin/bash
#
# This is the command I use in the vagrantVM to run all the tests in the directory that supports O_DIRECT
# and afterwards copy all test results back to the vagrant shared folder
# cd /vagrant && clear && make clean && make scanner_test && clear && make parser_test && rm -r /home/vagrant/bin/ && cp -r bin/ /home/vagrant/ && clear && cd /home/vagrant/bin/ && run_all_tests.sh ; cp -r test/test_output/ /vagrant/bin/test/

scanner_test() {
	echo "###########################"
	echo "RUNGING SCANNER TESTS"
	echo "###########################"

	for i in {1..10}
	do
	  echo "Running test number $i"
	  ./scanner_test test/testfiles/scanner/scanner$i.txt test/test_output/scanner/output$i.txt
	done
}

parser_test() {
	echo "###########################"
	echo "RUNGING PARSER TESTS"
	echo "###########################"

	# do not run test4 as it only makes sense with floats (that we do not have this semester)
	for i in {0,1,2,3,5,6,7,8,9,10,11,12}
	do
	  echo "Running test number $i"
	  # let the scanner test parse the parser test files to see if a problem may be caused by the scanner
	  ./scanner_test test/testfiles/parser/parser$i.txt test/test_output/parser/scanner$i.txt
	  # let the parser parse the file and output the parse_tree as JSON and output the code in the code.txt
	  ./parser_test test/testfiles/parser/parser$i.txt test/test_output/parser/json$i.txt test/test_output/parser/json_typeCheck$i.txt test/test_output/parser/code$i.txt
	  # push the json through a python tool that formats the JSON to pretty JSON
	  cat test/test_output/parser/json$i.txt | python -mjson.tool > test/test_output/parser/json_pretty$i.txt
	  cat test/test_output/parser/json_typeCheck$i.txt | python -mjson.tool > test/test_output/parser/json_typeCheck_pretty$i.txt
	done
}

# calling the tests here
scanner_test
parser_test

exit 0
\chapter{Optimierung}
\label{chap:optimize}
Mit Hilfe von Parallelisierung versuchen wir die Effizienz zu steigern, d.h. die Laufzeit des Programms auch bei großen Text-Dateien zu verbessern.

\section{Welche Optimierunsmöglichkeiten gibt es?}
\label{sec:possibilities_optimize}
Zum einen kann man den Puffer parallelisieren, indem man den Schreib- und Leseprozess parallel laufen lässt. Dabei muss man mindestens drei Puffer-Segmente haben, da der Leser immer zwei Segmente sperrt, um zurücklaufen zu können. Der Schreiber kann dann auf dem dritten Segment schreiben. Diese Optimierung haben wir auch implementiert.

Zum anderen kann man im Parser den Baum aufteilen. Ohne Optimierung wird zunächst der Baum aufgebaut, dann überprüft der Typchecker den erstellten Baum und kann noch Änderungen an diesem vornehmen und zum Schluss wird dann der Code generiert und in eine Datei geschrieben. Sobald der Typechecker für eine Seite des Baums durchgelaufen ist, wird diese nicht mehr verändert. Der Codegenerator läuft nun bereits über die linke Seite(\texttt{Declarations}), während  der Baum für die rechte Seite (\texttt{Statements}) erstellt und überprüft wird. Dadurch wird die Generierung des Codes der linken Seite parallel zur Erstellung und Überprüfung der rechten Seite durchgeführt. 

\section{Erzeuger-Verbraucher-Konzept}
\label{sec:producer_consumer}
Der Puffer wird nach dem Erzeuger-Verbraucher-Konzept optimiert. Der Leser ist dabei der Verbraucher und der Schreiber der Erzeuger. Der Prozess wird also vom \texttt{Writer} erzeugt und füllt ein Segment des Datenpuffers. Anschließend wird der \texttt{Reader} informiert, so dass dieser die Daten verbrauchen, d.h. verarbeiten kann, um sie danach wieder freizugeben.

\section{Implementierungsdetails}
\label{sec:optimize_details}
Wir arbeiten mit Posix-Threads. Diese sind plattformunabhängig und bieten gängige Synchronisierungsoptionen direkt in der API an. Synchronisierung brauchen wir deshalb, weil zum Beispiel ein Puffersegmet, das gerade gelesen wird nicht im selben Moment von \texttt{Writer-Thread} überschrieben werden darf.

Der Puffer wird in drei Segmente geteilt. Für jedes Segment gibt es sowohl ein \texttt{Readlock} als auch ein \texttt{Writelock} (alles Mutexe). Die \texttt{Writelocks} legen fest, ob der \texttt{Writer-Thread} in entsprechende Segment schreiben darf oder nicht. Die \texttt{Readlocks} legen fest, ob aus dem Segment vom \texttt{Reader-Thread} gelesen werden darf.

\begin{lstlisting}
pthread_mutex_t read_mutex [SEGMENT_COUNT]; // read mutex for each buffer segment
pthread_mutex_t write_mutex [SEGMENT_COUNT]; // write mutex for each buffer segment
\end{lstlisting}

Zu Beginn sind alle Segmente für den \texttt{Reader} gesperrt, da in keines der Segmente Daten geladen wurden, die gelesen werden könnten. Dem \texttt{Writer} stehen allerdings alle Segmente frei - er soll schließlich die Segmente nun mit Daten befüllen.

\begin{lstlisting}
// initialize mutexes
for(int i = 0; i < SEGMENT_COUNT; i++) {
	pthread_mutex_init(&read_mutex[i], NULL);
	pthread_mutex_lock(&read_mutex[i]); // lock readlocks
	pthread_mutex_init(&write_mutex[i], NULL);
}
\end{lstlisting}

\subsection{Writer Thread}
Läuft in einer Schleife immer wieder über die Puffersegmente. Falls das \texttt{Writelock} nicht gesperrt ist, füllt er das Segment mit Daten. Dabei wird das \texttt{Writelock} gesperrt und das \texttt{Readlock} entsperrt. Nun kann der \texttt{Reader} kommen und Daten aus dem Segment lesen.

\begin{lstlisting}
void Buffer::writerThreadMethod() {
	int current_buffer = 0;
	while(true) {
		pthread_mutex_lock(&write_mutex[current_buffer]);
		fillBuffer(SINGLE_BUFFER_SIZE * current_buffer);
		pthread_mutex_unlock(&read_mutex[current_buffer]);
		// go to next segment
		current_buffer = (current_buffer + 1) % SEGMENT_COUNT;
	}
}
\end{lstlisting}

Gestartet wird der neue \texttt{Writer-Thread} im Konstruktor des Puffers:

\begin{lstlisting}
// create writer thread
if(pthread_create(&buffer_writer_thread, NULL, writerThreadStart, this)) {
	// thread creation failed, return value is not 0
	STDERR("Could not create BufferWriter thread.");
}
\end{lstlisting}

Allerdings muss dieser Methode eine anonyme Methode übergeben werden. In diesem Fall \texttt{writerThreadStart}. Zusätzlich können Attribute mitgegeben werden. Allerdings soll unser \texttt{Writer-Thread} Zugriff auf alle Attribute des Puffers haben. Daher geben wir als Attribut einfach \texttt{this} mit und rufen in der anonymen Methode dann eine Objektmethode auf - die dann wieder Zugriff auf alle Attribute des Puffers hat.

\begin{lstlisting}
void* writerThreadStart(void* data) {
	Buffer* buffer = static_cast<Buffer*> (data);
	buffer->writerThreadMethod();
	pthread_exit(0);
}
\end{lstlisting}

\subsection{Reader Thread}
Ist eigentlich kein eigener Thread, sondern der Main-Thread des Programms, der mit \texttt{getChar()} immer wieder Zeichen vom Puffer anfordert. Allerdings muss dies trotzdem mit dem \texttt{Writer} synchronisiert werden.

Der \texttt{Reader} wandert zeichenweise durch den Puffer. Er muss nun immer beim Betreten eines neuen Puffersegments prüfen ob das Mutex freigegeben ist - ansonsten muss er warten, bis der \texttt{Writer} das Segment befüllt und es freigegeben hat.

Sobald der \texttt{Reader} ein neues Segment betritt, gibt er das Segment, das zwei zurück liegt wieder für den \texttt{Writer} frei. Das letzt Segment muss noch für den \texttt{Reader} reserviert bleiben, da im Falle eines \texttt{ungetChar()} dorthin zurück gesprungen werden muss.

\begin{lstlisting}
if(offset % SINGLE_BUFFER_SIZE == 0 && buffer_segment == next_read_segment) {
	// we are at the start of a new segment
	// therefore we can only continue to read chars if the read lock mutex is free
	pthread_mutex_lock(&read_mutex[buffer_segment]);

	// additionally the write lock two segments back has to be released
	int release_segment = buffer_segment - 2;
	if (release_segment < 0)
		release_segment = SEGMENT_COUNT + release_segment; // add here because release_segment is already negative
	pthread_mutex_unlock(&write_mutex[release_segment]);
}
\end{lstlisting}

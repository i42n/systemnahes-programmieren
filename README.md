#Systemnahes Programmieren Wintersemester 2013

Das Projekt wurde in 4 Teilprojekte aufgeteilt:

* Automat
* Buffer
* Symboltabelle	
* Scanner
* Parser

##Sprache die der Automat erkennt

```
digit:		0-9
letter:		a-z,A-Z
sign:		+ | - | / | * | < | > | = | == | =!= | ! | & | ; | ( | ) | { | } | [ | ]
integer:	digit digit*
identifier:	letter (letter | digit)*
if:		if | IF
while:		while | WHILE
```

Dabei entstehen folgende Tokens:

```
(NEWLINE)
(SPACE)
(UNKNOWN)
(COMMENT)

IDENTIFIER
INTEGER
WHILE
IF
ELSE
PRINT
READ

(SIGN)
SIGN_PLUS
SIGN_MINUS
SIGN_SLASH
SIGN_STAR
SIGN_SMALLER
SIGN_LARGER
SIGN_EQUAL
SIGN_DOUBLE_EQUAL
SIGN_UNEQUAL
SIGN_NOT
SIGN_AND
SIGN_SEMICOLON

SQUARE_BRACKET_OPEN
SQUARE_BRACKET_CLOSE
PARANTHESE_OPEN
PARANTHESE_CLOSE
CURLY_BRACKET_OPEN
CURLY_BRACKET_CLOSE
```